SET foreign_key_checks = 0;
drop table financial_institutions;
drop table payment_history;
drop table units;
drop table tenants;
drop table properties;
drop table mortgages;
drop table contractors;
drop table maintenances;
SET foreign_key_checks = 1;


CREATE TABLE IF NOT EXISTS properties
(property_id varchar(10), address varchar(50) ,type varchar(20), unit int, restrictions varchar(500), date_ownership date, 
ispaid boolean, school_taxe double, property_tace double, insurance double, PRIMARY KEY(property_id));

CREATE TABLE IF NOT EXISTS tenants
(tenant_id varchar(10), first_name varchar(50), last_name varchar(50), 
property_id varchar(10), unit_id varchar(10), email varchar(100), 
phone varchar(10), monthty_rent double, 
PRIMARY KEY(tenant_id), FOREIGN KEY(property_id) REFERENCES properties(property_id));

CREATE TABLE IF NOT EXISTS units
(unit_id varchar(10), property_id varchar(10), tenant_id varchar(10), gas double, 
electricity double, condoFees double, PRIMARY KEY(unit_id,property_id), 
FOREIGN KEY(property_id) REFERENCES properties(property_id),
FOREIGN KEY(tenant_id) REFERENCES tenants(tenant_id));

ALTER TABLE tenants ADD FOREIGN KEY(unit_id) REFERENCES units(unit_id);

CREATE TABLE IF NOT EXISTS financial_institutions
(fi_id varchar(10), name varchar(50), interest_rate double, PRIMARY KEY(fi_id));

CREATE TABLE IF NOT EXISTS mortgages
(mortgage_id varchar(10), property_id varchar(10), down_payment double, 
amount_payed double, amout_left double, fi_id varchar(10), interest_rate double, 
start_mortgage date, end_mortgage date, monthly_payment double, 
PRIMARY KEY(mortgage_id), FOREIGN KEY(property_id) REFERENCES properties(property_id),
FOREIGN KEY(fi_id) REFERENCES financial_institutions(fi_id));

CREATE TABLE IF NOT EXISTS payment_history
(payment_id varchar(10), tenant_id varchar(10), payDate date, method varchar(20), 
amount double, PRIMARY KEY(payment_id), FOREIGN KEY(tenant_id) REFERENCES tenants(tenant_id));

