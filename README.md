# Real Estate Manager

This application is programmed in Java. 
It allows the user to manage its properties. They can add, modify and delete a tenants, a mortgage, a contractor and a property. It also allows the user to manage the tenants' payment, the tenant's lease, the dates and costs of maintenance, and the properties' maintenance.
