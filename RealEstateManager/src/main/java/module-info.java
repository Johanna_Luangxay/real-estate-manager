module com.johanna.realestatemanager {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires java.desktop;

    opens com.johanna.realestatemanager to javafx.fxml, java.sql, java.desktop;
    exports com.johanna.realestatemanager;
    opens com.johanna.realestatemanager.controllers to javafx.fxml, java.sql, java.desktop;
    exports com.johanna.realestatemanager.controllers;
    opens com.johanna.realestatemanager.models to javafx.fxml, java.sql, java.desktop;
    exports com.johanna.realestatemanager.models;
}