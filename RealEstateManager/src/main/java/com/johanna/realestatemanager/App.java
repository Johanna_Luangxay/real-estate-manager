package com.johanna.realestatemanager;

import com.johanna.realestatemanager.models.DBManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.sql.SQLException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("Properties"), 750, 600);
        stage.setTitle("Real Estate Manager");
        stage.setScene(scene);
        stage.show();
    }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
       FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
       
       return fxmlLoader.load();
     }

    public static Scene getScene(){
        return scene;
    }
    
    public static void main(String[] args) throws SQLException {
        try{
            DBManager db = new DBManager(); 
            db.createAllTables();
            launch();
        }
        catch(SQLException e){
            System.out.println("Error: \n"+e);
        }
    }

}