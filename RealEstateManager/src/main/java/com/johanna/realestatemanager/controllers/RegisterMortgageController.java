package com.johanna.realestatemanager.controllers;

import com.johanna.realestatemanager.App;
import com.johanna.realestatemanager.models.FinancialInstitution;
import com.johanna.realestatemanager.models.FinancialInstitutionModel;
import com.johanna.realestatemanager.models.Mortgage;
import com.johanna.realestatemanager.models.MortgageModel;
import com.johanna.realestatemanager.models.Property;
import com.johanna.realestatemanager.models.PropertyModel;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

/**
 *
 * @author Johanna
 */
public class RegisterMortgageController extends AbstractInfo{
    
    private boolean isNew;
    
    private Mortgage currentMortgage;
    
    @FXML
    private ComboBox input_address;

    @FXML
    private TextField input_value;

    @FXML
    private TextField input_downPay;

    @FXML
    private TextField input_amountLeft;

    @FXML
    private ComboBox input_fi;

    @FXML
    private TextField input_rate;

    @FXML
    private DatePicker input_startDate;

    @FXML
    private DatePicker input_endDate;

    @FXML
    private TextField input_monthPay;
    
    /**
     * Set if the mortgage is brand new or a mortgage to view/modify
     * @param isNew boolean for if its new mortgage
     */
    public void setNew(Boolean isNew){
        this.isNew = isNew;
    }
    
    /**
     * Set current mortgage
     * @param newMortgage mortgage to set as current mortgage
     */
    public void setCurrentMortgage(Mortgage newMortgage){
        this.currentMortgage = newMortgage;
    }
    
    /**
     * Is called when view is loaded. Initialize some input fields.
     */
    @FXML
    public void initialize(){
        initComboBox();
        initTextField();
    }
    
    /**
     * Initialize combobox to have the right list to choose from
     */
    private void initComboBox(){
    	//Combobox for financial institution list
        ObservableList<String> fiList = FXCollections.observableArrayList();
        ArrayList<FinancialInstitution> dbFiList = FinancialInstitutionModel.getFiList();
        for(FinancialInstitution fi : dbFiList){
            fiList.add(fi.getName());
        }
        input_fi.setItems(fiList);
        
        //Combobox for property to choose from
        ObservableList<String> propertyList = FXCollections.observableArrayList();
        ArrayList<Property> dbPropertyList = PropertyModel.getPropertyList();
        for(Property property : dbPropertyList){
            propertyList.add(property.getAddress());
        }
        input_address.setItems(propertyList);
    }
    
    /**
     * Initialize textfield to reject undesired character
     */
    private void initTextField(){
        Validators.validateNumber(input_value);
        Validators.validateNumber(input_downPay);
        Validators.validateNumber(input_rate);
        Validators.validateNumber(input_monthPay);
    }
    
    /**
     * Display data of current mortgage
     */
    public void loadData(){
        if(!isNew){
            input_address.setValue(PropertyModel.getPropertyAddress(currentMortgage.getPropertyId()));
            input_value.setText(Double.toString(currentMortgage.getValue()));
            input_downPay.setText(Double.toString(currentMortgage.getDownPayment()));
            input_amountLeft.setText(Double.toString(currentMortgage.getAmountLeft()));
            input_fi.setValue(FinancialInstitutionModel.getFiName(currentMortgage.getFiId()));
            input_rate.setText(Double.toString(currentMortgage.getRate()));
            input_startDate.setValue(currentMortgage.getStartDate());
            input_endDate.setValue(currentMortgage.getEndDate());
            input_monthPay.setText(Double.toString(currentMortgage.getMonthPay()));
        }
    }
    
    /**
     * Confirms user wants to exit and leads them to the mortgage page
     */
    @Override
    public void exit() throws IOException{
        Alert alert = showConfirmation("Are you sure you want to exit? (Don't forget to save!)");
        
        Optional<ButtonType> option = alert.showAndWait();
      
        if (option.get() == ButtonType.OK) {
            App.setRoot("Mortgages");
        }
    }
    
    /**
     * Calls model to save inputs in database
     */
    @Override
    public void save(){
        Alert alert = showConfirmation("Are you sure you want to change the informations?");
        
        Optional<ButtonType> option = alert.showAndWait();
        
        if (option.get() == ButtonType.OK) {
            if(isNew){
                addMortgage();
                loadData();
            }else{
                modifyMortgage();
                loadData();
            }
        }
    }
    
    /**
     * Update current mortgage with input data
     */
    private void updateData() throws Exception {
        //Id
        String mortgageId;
        if(isNew){
            mortgageId = MortgageModel.getLatestId();
        }
        else{
            mortgageId = currentMortgage.getMortgageId();
        }
        //Property
        String propertyId;
        try{
            propertyId = PropertyModel.getPropertyId(input_address.getSelectionModel().getSelectedItem().toString());
        }
        catch(NullPointerException e){
            showError("You did not choose a property!");
            throw new Exception("No property selected.");
        }
        //Down Payment
        double downPayment;
        try{
            downPayment = Double.parseDouble(input_downPay.getText());
        }
        catch(NumberFormatException e){
            downPayment = 0.00;
        }
        //Property value
        double value;
        try{
            value = Double.parseDouble(input_value.getText());
        }
        catch(NumberFormatException e){
            value = 0.00;
        }
        //Amount left
        double amountLeft;
        try{
            amountLeft = Double.parseDouble(input_amountLeft.getText());
        }
        catch(NumberFormatException e){
            amountLeft = 0.00;
        }
        //Financial Institution
        String fiId ;
        try{
            fiId = FinancialInstitutionModel.getFiId(input_fi.getSelectionModel().getSelectedItem().toString());
        } 
        catch(NullPointerException e){
            showError("You did not choose a financial institution!");
            throw new Exception("No financial institution selected.");
        }
        double rate;
        //Rate
        try{
            rate = Double.parseDouble(input_rate.getText());
        }
        catch (NumberFormatException e){
            rate = 0.00;
        }
        //Start Date
        LocalDate startDate = input_startDate.getValue();
        //End Date
        LocalDate endDate = input_endDate.getValue();
        //Monthly Payment
        double monthPay;
        try{
            monthPay = Double.parseDouble(input_monthPay.getText());
        }
        catch (NumberFormatException e){
            monthPay = 0.00;
        }
        
        currentMortgage = new Mortgage(mortgageId, propertyId, downPayment, value, 
                amountLeft, fiId, rate, startDate, endDate, monthPay);
        
        validateData();
    }
    
    /**
     * Validate Data
     * @throws Exception 
     */
    private void validateData() throws Exception{
        if(currentMortgage.getStartDate() == null){
            Alert alert = showConfirmation("You did not enter the start date. Do you want to put today's date?");
            Optional<ButtonType> option = alert.showAndWait();
        
            if (option.get() == ButtonType.OK) {
                currentMortgage.setStartDate(LocalDate.now());         
            } 
            else {
                showWarning("Please set the start date");               
                throw new Exception("No input for start date.");
            }
        }
        
        if(currentMortgage.getEndDate() == null){
            Alert alert = showConfirmation("You did not enter the end date. Do you want to put 1 year from today?");
            Optional<ButtonType> option = alert.showAndWait();
        
            if (option.get() == ButtonType.OK) {
                currentMortgage.setEndDate(LocalDate.now().plusYears(1));         
            } 
            else {
                showWarning("Please set the end date");               
                throw new Exception("No input for end date.");
            }
        }
    }
    
    /**
     * Calls model to add new mortgage
     */
    private void addMortgage(){
        try{
            updateData();
            MortgageModel.addMortgage(currentMortgage);
            isNew = false;
            System.out.println("Mortgage added!");
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
    
    /**
     * Calls model to modify mortgage
     */
    private void modifyMortgage(){
        try{
            updateData();
            MortgageModel.modifyMortgage(currentMortgage);
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
}
