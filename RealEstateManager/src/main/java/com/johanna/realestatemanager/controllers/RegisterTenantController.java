package com.johanna.realestatemanager.controllers;

import com.johanna.realestatemanager.App;
import com.johanna.realestatemanager.models.*;
import java.awt.Desktop;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
 import java.io.File;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Takes care of registration tenant view
 * @author Johanna
 */
public class RegisterTenantController extends AbstractInfo{
    
    private boolean isNew;
    
    private Tenant currentTenant;
    
    @FXML
    private ListView<File> leaseList;
    
    @FXML
    private TextField input_fname;

    @FXML
    private TextField input_lname;

    @FXML
    private TextField input_phone;

    @FXML
    private TextField input_email;

    @FXML
    private ComboBox input_address;

    @FXML
    private ComboBox input_unit;

    @FXML
    private TextField input_cost;

    @FXML
    private TextField input_rate;
    
    /**
     * Set boolean of whether the current teannt is a new tenant
     * @param isNew boolean value to be set 
     */
    public void setNew(boolean isNew){
        this.isNew = isNew;
    }
    
    /**
     * Set current tenant object
     * @param newTenant tenant to be set as current tenant
     */
    public void setCurrentTenant(Tenant newTenant){
        this.currentTenant = newTenant;
    }
    
    /**
     * Is called when the view is loaded. Initialize some input fields
     */
    @FXML
    public void initialize(){
        initComboBox();
        initTextField();
    }
    
    /**
     * Initialize combobox 
     */
    private void initComboBox(){
    	//For listing the existing properties
        ObservableList<String> propertyList = FXCollections.observableArrayList();
        ArrayList<Property> dbPropertyList = PropertyModel.getPropertyList();
        for(Property property : dbPropertyList){
            propertyList.add(property.getAddress());
        }
        input_address.setItems(propertyList);
    }
    
    /**
     * Initialize the list of unit depending on the proeprty selected
     */
    @FXML
    public void initUnitCombo(){
        ObservableList<String> unitList = FXCollections.observableArrayList();
        String address = input_address.getSelectionModel().getSelectedItem().toString();
        String propertyId = PropertyModel.getPropertyId(address);
        ArrayList<String> dbUnitList = PropertyModel.getUnitList(propertyId);
        for(String unit : dbUnitList){
            unitList.add(unit);
        }
        input_unit.setItems(unitList);
        input_unit.getSelectionModel().selectFirst();
    }
    
    /**
     * Initialize the text field to reject undesirable character
     */
    private void initTextField(){
        Validators.validatePhone(input_phone);
        Validators.validateName(input_fname);
        Validators.validateName(input_lname);
        Validators.validateNumber(input_cost);
        Validators.validateNumber(input_rate);
        Validators.validateEmail(input_email);
    }
    
    /**
     * Confirms exit and leads user to tenant view
     */
    @Override
    public void exit() throws IOException{
        Alert alert = showConfirmation("Are you sure you want to exit? (Don't forget to save!)");
        
        Optional<ButtonType> option = alert.showAndWait();
      
        if (option.get() == ButtonType.OK) {
            removeNotSavedTenantLease();
            App.setRoot("Tenants");
        }
    }
    
    /**
     * Confirms saving and proceeds to it
     */
    @Override
    public void save(){
        Alert alert = showConfirmation("Are you sure you want to change the informations?");
        Optional<ButtonType> option = alert.showAndWait();
        
        if (option.get() == ButtonType.OK) {
            if(isNew){
                addTenant();
                loadData();
            }else{
                modifyTenant(); 
                loadData();
            }
        }
    }
    
    /**
     * Display data of current tenant
     */
    public void loadData(){
        if (!(isNew))
        {
            input_fname.setText(currentTenant.getFirstName());
            input_lname.setText(currentTenant.getLastName());
            input_phone.setText(currentTenant.getPhone());
            input_email.setText(currentTenant.getEmail());
            input_address.setValue(PropertyModel.getPropertyAddress(currentTenant.getPropertyId()));
            input_unit.setValue(currentTenant.getUnitId());
            input_cost.setText(Double.toString(currentTenant.getRent()));
            input_rate.setText(Double.toString(currentTenant.getRate()));
            loadLease();
        } 
    }
    
    /**
     * Load list of lease of teanant
     */
    private void loadLease(){
        String tenantId;
        if(isNew){
            tenantId = TenantModel.getLatestId();
        }
        else {
            tenantId = currentTenant.getTenantId();
        }
        File dir = new File("Lease/"+tenantId);
        if(dir.exists()){
            File[] fileList = dir.listFiles();
            leaseList.getItems().clear();
            leaseList.getItems().addAll(fileList);
        }
    }
    
    /**
     * Update current tenant object with user input
     */
    private void updateData() throws Exception {
        //Tenant id
        String tenantId;
        if(isNew){
            tenantId = TenantModel.getLatestId();
        }
        else{
            tenantId = currentTenant.getTenantId();
        }
        //First Name
        String fname = input_fname.getText();
        //Last Name
        String lname = input_lname.getText();
        //Phone Number
        String phone = input_phone.getText();
        //Email
        String email = input_email.getText();
        //Property
        String propertyId;
        try{
            propertyId = PropertyModel.getPropertyId(input_address.getSelectionModel().getSelectedItem().toString());
        }
        catch(NullPointerException e){
            showError("You did not choose a property!");
            throw new Exception("No property selected.");
        }
        //Unit
        String unit ;
        try{
            unit = input_unit.getSelectionModel().getSelectedItem().toString();
        }
        catch(NullPointerException e){
            showError("You did not choose a unit number");
            throw new Exception("No unit choosen");
        }
        //Rent Cost
        double cost;
        try{
            cost = Double.parseDouble(input_cost.getText());
        }
        catch(NumberFormatException e){
            cost = 0.00;
        }
        //Rate
        double rate;
        try{
            rate = Double.parseDouble(input_rate.getText());
        }
        catch(NumberFormatException e){
            rate = 0.00;
        }
        
        currentTenant = new Tenant(tenantId,fname,lname,propertyId,unit,email,phone,cost,rate);
        
        validateData();
    }
    
    /**
     * Validate user input
     */
    private void validateData() throws Exception{
        if(currentTenant.getFirstName().isEmpty()){
            showError("You did not enter the first name of the tenant!");
            throw new Exception("No input for the first name.");
        }
        if(currentTenant.getLastName().isEmpty()){
            showError("You did not enter the last name of the tenant!");
            throw new Exception("No input for the last name.");
        }
        if(currentTenant.getPhone().isEmpty()){
            Alert alert = showConfirmation("Are you sure you don't want to input the tenant's phone number?");
            Optional<ButtonType> option = alert.showAndWait();
        
            if (option.get() != ButtonType.OK) {
                throw new Exception("No phone input");
            }
        }
        
        if(currentTenant.getEmail().isEmpty()){
            Alert alert = showConfirmation("Are you sure you do not wish to put the tenant's email?");
            Optional<ButtonType> option = alert.showAndWait();
        
            if (option.get() != ButtonType.OK) {
                throw new Exception("No email input");
            }
        }
        else {
            String regex = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
            if(!currentTenant.getEmail().matches(regex)){
                showError("You did not enter a valid email!");
                throw new Exception("No valid email input.");
            }
        }
    }
   
    
    /**
     * Calls model to add tenant
     */
    private void addTenant(){
        try {
            updateData();
            TenantModel.addTenant(currentTenant);
            isNew = false;
            System.out.println("Tenant added!");
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Calls model to modify tenant
     */
    private void modifyTenant(){
        try {
            updateData();
            TenantModel.modifyTenant(currentTenant);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Add lease for the tenant
     */
    @FXML
    public void addLease(){
        FileChooser fc = new FileChooser();
        //Only allow PDF
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.PDF", "*.pdf");
        fc.getExtensionFilters().add(filter);
        
        File originalLease = fc.showOpenDialog(null);
        
        String tenantId;
        if(isNew){
            tenantId = TenantModel.getLatestId();
        }
        else {
            tenantId = currentTenant.getTenantId();
        }

        //Select directory
        File directory = new File("Lease/"+tenantId);
        //Make directory if directory for user do not exist
        if(! directory.exists()){
            directory.mkdir();
        }
        //Determines file name
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH.mm.ss");
        String filename = dateTime.format(formatter);
        //Make a copy of the file
        File copyLease = new File(directory + "/Lease - " +filename+".pdf");
        try{
            Files.copy(originalLease.toPath(),copyLease.toPath());
        }
        catch (Exception e){
            System.out.println("Copy Fail: "+e);
        }
        
        loadLease();
    }
    
    /**
     * Remove lease in directories
     */
    @FXML
    public void removeLease() {
        File selectedFile = leaseList.getSelectionModel().getSelectedItem();
        selectedFile.delete();
        loadLease();
    }
    
    /**
     * Open the pdf
     */
    @FXML
    void viewLease() {
        File selectedFile = leaseList.getSelectionModel().getSelectedItem();
        Desktop desktop = Desktop.getDesktop(); 
        try {
            desktop.open(selectedFile);
        } catch (IOException ex) {
            Logger.getLogger(RegisterTenantController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * If users quits before completing the tenant, it deletes the lease directory
     */
    private void removeNotSavedTenantLease(){
        if(isNew){
            File directoryToDelete = new File("Lease/"+TenantModel.getLatestId());
            if(directoryToDelete.exists()){
                String[] entryList = directoryToDelete.list();
                for(String entry : entryList){
                    File currentFile = new File(directoryToDelete.getPath(),entry);
                    currentFile.delete();
                }
                directoryToDelete.delete();
            }
        }
    }
}
