package com.johanna.realestatemanager.controllers;

import java.io.IOException;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * Class from view that takes in data extends from.
 * @author Johanna
 */
public abstract class AbstractInfo {
    
	/**
	 * Method to exit page
	 * @throws IOException
	 */
    @FXML
    public abstract void exit() throws IOException;
    
    /**
     * Method to save data
     * @throws IOException
     * @throws SQLException
     */
    @FXML
    public abstract void save() throws IOException, SQLException;
    
    /**
     * Method to confirm before saving or quitting
     * @param message
     * @return
     */
    @FXML
    protected Alert showConfirmation(String message){
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Are you sure?");
        alert.setHeaderText(message);
        
        return alert;
    }
    
    /**
     * Method to show error message
     * @param message
     * @return 
     */
    @FXML
    protected void showError(String message){
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(message);
        alert.show();
    }
    
    @FXML
    protected void showWarning(String message){
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setHeaderText(message);
        alert.show();
    }
    
    
}
