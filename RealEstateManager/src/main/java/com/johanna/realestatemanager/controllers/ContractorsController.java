package com.johanna.realestatemanager.controllers;

import com.johanna.realestatemanager.App;
import com.johanna.realestatemanager.models.Contractor;
import com.johanna.realestatemanager.models.ContractorModel;
import com.johanna.realestatemanager.models.DBManager;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

/**
 * Class that takes care of the contractor view
 * @author Johanna
 */
public class ContractorsController extends AbstractInfo{
    
    ObservableList<Contractor> contractor_data = FXCollections.observableArrayList();
    
    @FXML
    private TableView<Contractor> table_contractor;

    @FXML
    private TableColumn<Contractor, String> col_fname;

    @FXML
    private TableColumn<Contractor, String> col_lname;

    @FXML
    private TableColumn<Contractor, String> col_phone;

    @FXML
    private TableColumn<Contractor, String> col_email;

    @FXML
    private TableColumn<Contractor, String> col_type;

    /**
     * Method for when view is loaded
     * @throws SQLException
     */
    @FXML
    public void initialize() throws SQLException{
        initCol();
        loadTableData();
    }
    
    /**
     * Method that initialize the column of the table
     */
    private void initCol(){
        col_fname.setCellValueFactory(new PropertyValueFactory<>("fname"));
        col_lname.setCellValueFactory(new PropertyValueFactory<>("lname"));
        col_phone.setCellValueFactory(new PropertyValueFactory<>("phone"));
        col_email.setCellValueFactory(new PropertyValueFactory<>("email"));
        col_type.setCellValueFactory(new PropertyValueFactory<>("type"));
        
        editableCols();
    }
    
    /**
     *  Method that makes the column editable
     */
    private void editableCols(){
        col_fname.setCellFactory(TextFieldTableCell.forTableColumn());
        col_lname.setCellFactory(TextFieldTableCell.forTableColumn());
        col_phone.setCellFactory(TextFieldTableCell.forTableColumn());
        col_email.setCellFactory(TextFieldTableCell.forTableColumn());
        col_type.setCellFactory(TextFieldTableCell.forTableColumn());
        
        col_fname.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setFname(e.getNewValue());
        });
        col_lname.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setLname(e.getNewValue());
        });
        col_phone.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setPhone(e.getNewValue());
        });
        col_email.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setEmail(e.getNewValue());
        });
        col_type.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setType(e.getNewValue());
        });
        
        table_contractor.setEditable(true);
    }
    
    /**
     * Method that loads the data in the table
     * @throws SQLException
     */
    private void loadTableData() throws SQLException{
        table_contractor.getItems().clear();
        ContractorModel.getContractorList().forEach(contractor -> {
            contractor_data.add(contractor);
        });
        table_contractor.setItems(contractor_data);
    }
    
    /**
     * Method that add an editable row to the table
     */
    @FXML
    public void addRow() {
        int rowNum = table_contractor.getItems().size()+1;
        int latestId = ContractorModel.getLatestId();
        int id = 0;
        if(rowNum > latestId){
            id = rowNum;
        }
        else if (rowNum == latestId){
            id = rowNum+1;
        }
        else {
            id = latestId;
        }
        table_contractor.getItems().add(new Contractor(Integer.toString(id),"","","","",""));
    }
    
    /**
     * Method that removes a row in the table
     */
    @FXML
    public void removeRow() {
        Contractor selectedContractor = table_contractor.getSelectionModel().getSelectedItem();
        table_contractor.getItems().remove(selectedContractor);
    }
    
    /**
     * Method to confirm exit
     */
    @Override
    public void exit() throws IOException{
        Alert alert = showConfirmation("Are you sure you want to exit? (Don't forget to save!)");
        
        Optional<ButtonType> option = alert.showAndWait();
      
        if (option.get() == ButtonType.OK) {
            App.setRoot("Maintenances");
        }
    }
    
    /**
     * Method to save
     */
    @Override
    public void save() throws IOException{
        Alert alert = showConfirmation("Are you sure you want to change the informations?");
        
        Optional<ButtonType> option = alert.showAndWait();
        
        if (option.get() == ButtonType.OK) {
            DBManager.deleteAll("contractors");
            table_contractor.getItems().forEach(contractor -> {
                ContractorModel.addContractor(contractor);
            });
        }
    }
}
