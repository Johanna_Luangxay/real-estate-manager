package com.johanna.realestatemanager.controllers;

import com.johanna.realestatemanager.App;
import com.johanna.realestatemanager.models.*;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.LocalDateStringConverter;

/**
 * Takes care of payment history view
 * @author Johanna
 */
public class PaymentHistoryController extends AbstractInfo {

    ObservableList<PaymentHistory> payment_data = FXCollections.observableArrayList();
    
    @FXML
    private TableView<PaymentHistory> table_payment;

    @FXML
    private TableColumn<PaymentHistory, LocalDate> col_date;

    @FXML
    private TableColumn<PaymentHistory, String> col_tenant;

    @FXML
    private TableColumn<PaymentHistory, String> col_method;

    @FXML
    private TableColumn<PaymentHistory, Double> col_amount;

    /**
     * Method called when view is loaded
     * @throws SQLException
     */
    @FXML
    public void initialize() throws SQLException{
        initCol();
        loadTableData();
    }
    
    /**
     * Initialize column of the table
     */
    private void initCol(){
        col_date.setCellValueFactory(new PropertyValueFactory<>("payDate"));
        //Make tenant id display name instead
        col_tenant.setCellValueFactory(cellData -> {
            String tenantId = cellData.getValue().getTenantId();
            String tenantName = TenantModel.getTenantName(tenantId);
            return new ReadOnlyStringWrapper(tenantName);
            });
        col_method.setCellValueFactory(new PropertyValueFactory<>("method"));
        col_amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        
        editableCols();
    }
    
    /**
     * Makes cells of column editable
     */
    private void editableCols(){
        col_date.setCellFactory(TextFieldTableCell.forTableColumn(new LocalDateStringConverter()));
        col_tenant.setCellFactory(TextFieldTableCell.forTableColumn());
        col_method.setCellFactory(TextFieldTableCell.forTableColumn());
        col_amount.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        
        col_date.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setPayDate(e.getNewValue());
        });
        col_tenant.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setTenantId(TenantModel.getTenantId(e.getNewValue()));
        });
        col_method.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setMethod(e.getNewValue());
        });
        col_amount.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setAmount(e.getNewValue());
        });
        
        
        table_payment.setEditable(true);
    }
    
    /**
     * Load data from database into table
     * @throws SQLException
     */
    private void loadTableData() throws SQLException{
        table_payment.getItems().clear();
        PaymentHistoryModel.getPaymentList().forEach(payment -> {
            payment_data.add(payment);
        });
        table_payment.setItems(payment_data);
    }
    
    /**
     * Add an editable row to the table
     */
    @FXML
    public void addRow() {
        int rowNum = table_payment.getItems().size()+1;
        int latestId = PaymentHistoryModel.getLatestId();
        int id = 0;
        if(rowNum > latestId){
            id = rowNum;
        }
        else if (rowNum == latestId){
            id = rowNum+1;
        }
        else {
            id = latestId;
        }
        table_payment.getItems().add(new PaymentHistory(Integer.toString(id),"",LocalDate.now(),"",0));
    }
    
    /**
     * Remove a row
     */
    @FXML
    public void deleteRow(){
        PaymentHistory selectedPayment = table_payment.getSelectionModel().getSelectedItem();
        table_payment.getItems().remove(selectedPayment);
    }
    
    /**
     * Leads user to exit and go back to tenant page
     */
    @Override
    public void exit() throws IOException{
        Alert alert = showConfirmation("Are you sure you want to exit? (Don't forget to save!)");
        
        Optional<ButtonType> option = alert.showAndWait();
      
        if (option.get() == ButtonType.OK) {
            App.setRoot("Tenants");
        }
    }
    
    /**
     * Method that saves input in database
     */
    @Override
    public void save(){    
        Alert alert = showConfirmation("Are you sure you want to change the informations?");
        Optional<ButtonType> option = alert.showAndWait();
        
        if (option.get() == ButtonType.OK) {
            DBManager.deleteAll("payment_history");
            table_payment.getItems().forEach(payment -> {
                PaymentHistoryModel.addPayment(payment);
            });
        }
    }
    
}
