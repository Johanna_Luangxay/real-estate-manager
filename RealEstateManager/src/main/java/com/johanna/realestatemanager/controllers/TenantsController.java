package com.johanna.realestatemanager.controllers;

import com.johanna.realestatemanager.App;
import com.johanna.realestatemanager.models.DBManager;
import com.johanna.realestatemanager.models.Tenant;
import com.johanna.realestatemanager.models.TenantModel;
import java.io.*;
import java.sql.SQLException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;


/**
 * Taks care of the tenant view
 * @author Johanna
 */
public class TenantsController extends AbstractMenu{
    
    @FXML
    private TableView<Tenant> table_tenant;

    @FXML
    private TableColumn<Tenant, String> col_fname;

    @FXML
    private TableColumn<Tenant, String> col_lname;

    @FXML
    private TableColumn<Tenant, String> col_phone;

    @FXML
    private TableColumn<Tenant, String> col_email;
    
    ObservableList<Tenant> tenant_data = FXCollections.observableArrayList();
    
    Tenant selectedTenant;
    
    /**
     * Is called when view is loaded
     * @throws SQLException
     */
    @FXML
    public void initialize() throws SQLException{
        initCol();
        loadTableData();
    }
    
    /**
     * Initialize column of table
     */
    private void initCol(){
        col_fname.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        col_lname.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        col_email.setCellValueFactory(new PropertyValueFactory<>("email"));
        //Make fun number more pretty
        col_phone.setCellValueFactory(cellData -> {
            String number = cellData.getValue().getPhone();
            String phoneNumber = number.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
            return new ReadOnlyStringWrapper(phoneNumber);
            });
    }
    /**
     * Load table with data from database
     */
    private void loadTableData(){
        table_tenant.getItems().clear();
        for (Tenant tenant : TenantModel.getTenantList()){
            tenant_data.add(tenant);
        }
        table_tenant.setItems(tenant_data);
    }
    
    /**
     * Confirms removal of tenants and call model to delete entry
     */
    @Override @FXML
    public void removeEntry() {
        Alert alert = showConfirmation("Are you sure you want to delete this tenant?");
        
        Optional<ButtonType> option = alert.showAndWait();
        
        if (option.get() == ButtonType.OK) {
            selectedTenant = table_tenant.getSelectionModel().getSelectedItem();
            DBManager.removeEntry(selectedTenant.getTenantId(),"tenant_id","tenants");
            loadTableData();
        }
    }
    
    /**
     * Calls registration to display info and allow modification of selected tenant
     */
    @Override @FXML
    public void modifyEntry(){
        selectedTenant = table_tenant.getSelectionModel().getSelectedItem();
        try {
            FXMLLoader loader = new FXMLLoader(App.class.getResource("RegisterTenant.fxml"));
            Parent root = (Parent) loader.load();
            RegisterTenantController controller = loader.getController();
            controller.setCurrentTenant(selectedTenant);
            controller.setNew(false);
            controller.loadData();
            App.getScene().setRoot(root);
        } catch (IOException ex) {
            Logger.getLogger(PropertiesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Leads to registration page to add new tenant
     */
    @Override @FXML
    public void addEntry() throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("RegisterTenant.fxml"));
        Parent root = (Parent) loader.load();
        RegisterTenantController controller = loader.getController();
        controller.setNew(true);
        App.getScene().setRoot(root);
    }
    
    /**
     * Leads to the payment history
     * @throws IOException
     */
    @FXML
    private void switchToPaymentHistory() throws IOException {
        App.setRoot("PaymentHistory");
    }
}
