package com.johanna.realestatemanager.controllers;

import com.johanna.realestatemanager.App;
import com.johanna.realestatemanager.models.DBManager;
import com.johanna.realestatemanager.models.Property;
import com.johanna.realestatemanager.models.PropertyModel;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.logging.*;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.*;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;


/**
 * Takes care of the property view.
 * @author Johanna
 */
public class PropertiesController extends AbstractMenu{
    
    @FXML
    private TableView<Property> propertyList;
    
    @FXML
    private TableColumn<Property, String> col_address;

    @FXML
    private TableColumn<Property, String> col_type;

    @FXML
    private TableColumn<Property, LocalDate> col_ownership;

    @FXML
    private TableColumn<Property, String> col_status;
    
    Property selectedProperty;
    
    ObservableList<Property> property_data = FXCollections.observableArrayList();
    
    /**
     * Is called when view is called
     * @throws SQLException
     */
    @FXML
    public void initialize() throws SQLException{
        initCol();
        loadTableData();
    }
    
    /**
     * Initialize column of the table
     */
    private void initCol(){
        col_address.setCellValueFactory(new PropertyValueFactory<>("address"));
        col_type.setCellValueFactory(new PropertyValueFactory<>("type"));
        col_ownership.setCellValueFactory(new PropertyValueFactory<>("dateOfOwnership"));
        col_status.setCellValueFactory(cellData -> {
            boolean status = cellData.getValue().isIsPaid();
            String statusString = Property.convertStatusToWord(status);
            return new ReadOnlyStringWrapper(statusString);
            });
    }
    
    /**
     * Load data from db into the table
     */
    private void loadTableData(){
        propertyList.getItems().clear();
        for (Property property : PropertyModel.getPropertyList()){
            property_data.add(property);
        }
        propertyList.setItems(property_data);
    }
    
    /**
     * Remove a property from database
     */
    @Override @FXML
    public void removeEntry() {
        Alert alert = showConfirmation("Are you sure you want to delete this property?");
        
        Optional<ButtonType> option = alert.showAndWait();
        
        if (option.get() == ButtonType.OK) {
        selectedProperty = propertyList.getSelectionModel().getSelectedItem();
            DBManager.removeEntry(selectedProperty.getId(),"property_id","properties");
            loadTableData();
        }
    }
    
    /**
     * Leads user to view information on the property and allow modification
     */
    @Override @FXML
    public void modifyEntry(){
        selectedProperty = propertyList.getSelectionModel().getSelectedItem();
        try {
        	//Pass on the selected property to registration controller
            FXMLLoader loader = new FXMLLoader(App.class.getResource("RegisterProperty.fxml"));
            Parent root = (Parent) loader.load();
            RegisterPropertyController controller = loader.getController();
            controller.setCurrentProperty(selectedProperty);
            controller.setNew(false);
            controller.loadData();
            App.getScene().setRoot(root);
        } catch (IOException ex) {
            Logger.getLogger(PropertiesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Leads user to registration view to add new property
     */
    @Override @FXML
    public void addEntry() throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("RegisterProperty.fxml"));
        Parent root = (Parent) loader.load();
        RegisterPropertyController controller = loader.getController();
        controller.setNew(true);
        App.getScene().setRoot(root);
    }
}
