package com.johanna.realestatemanager.controllers;

import com.johanna.realestatemanager.App;
import com.johanna.realestatemanager.models.ContractorModel;
import com.johanna.realestatemanager.models.DBManager;
import com.johanna.realestatemanager.models.Maintenance;
import com.johanna.realestatemanager.models.MaintenanceModel;
import com.johanna.realestatemanager.models.PropertyModel;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;


/**
 * Class that takes care of the maintenance view
 * @author Johanna
 */
public class MaintenancesController extends AbstractMenu{
    
    private Maintenance selectedMaintenance;
    
    ObservableList<Maintenance> maintenance_data = FXCollections.observableArrayList();
    
    @FXML
    private TableView<Maintenance> table_maintenance;

    @FXML
    private TableColumn<Maintenance, String> col_name;

    @FXML
    private TableColumn<Maintenance, String> col_address;

    @FXML
    private TableColumn<Maintenance, LocalDate> col_date;

    @FXML
    private TableColumn<Maintenance, LocalTime> col_time;

    @FXML
    private TableColumn<Maintenance, Double> col_cost;
    
    /**
     * Method called when view is loaded
     */
    @FXML
    public void initialize(){
        initCol();
        loadTableData();
    }
    
    /**
     * Method that initialize the columns of the table
     */
    private void initCol(){
        col_name.setCellValueFactory(cellData -> {
            String id = cellData.getValue().getContractorId();
            String name = ContractorModel.getContractorName(id);
            return new ReadOnlyStringWrapper(name);
        });
        col_address.setCellValueFactory(cellData -> {
            String id = cellData.getValue().getPropertyId();
            String address = PropertyModel.getPropertyAddress(id);
            return new ReadOnlyStringWrapper(address);
        });
        col_date.setCellValueFactory(new PropertyValueFactory<>("date"));
        col_time.setCellValueFactory(new PropertyValueFactory<>("time"));
        col_cost.setCellValueFactory(new PropertyValueFactory<>("cost"));
    }
    
    /**
     * Method to load the table with data from Database
     */
    private void loadTableData(){
        table_maintenance.getItems().clear();
        for (Maintenance maintenance : MaintenanceModel.getMaintenanceList()){
            maintenance_data.add(maintenance);
        }
        table_maintenance.setItems(maintenance_data);
    }
    
    /**
     * Method to remove an entry from database
     */
    @Override @FXML
    public void removeEntry() {
        Alert alert = showConfirmation("Are you sure you want to delete this maintenance?");
        
        Optional<ButtonType> option = alert.showAndWait();
        
        if (option.get() == ButtonType.OK) {
        selectedMaintenance = table_maintenance.getSelectionModel().getSelectedItem();
        DBManager.removeEntry(selectedMaintenance.getMaintenanceId(),"maintenance_id","maintenances");
        loadTableData();
        }
    }
    
    /**
     * Method that brings user to registration of maintenance for new maintenance
     */
    @Override @FXML
    public void addEntry() throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("RegisterMaintenance.fxml"));
        Parent root = (Parent) loader.load();
        RegisterMaintenanceController controller = loader.getController();
        controller.setNew(true);
        App.getScene().setRoot(root);
    }
    
    /**
     * Method that brings user to see information on the maintenance and allow to modify 
     */
    @Override @FXML
    public void modifyEntry(){
        selectedMaintenance = table_maintenance.getSelectionModel().getSelectedItem();
        try {
        	//Pass selected maintenance to registration controller
            FXMLLoader loader = new FXMLLoader(App.class.getResource("RegisterMaintenance.fxml"));
            Parent root = (Parent) loader.load();
            RegisterMaintenanceController controller = loader.getController();
            controller.setCurrentMaintenance(selectedMaintenance);
            controller.setNew(false);
            controller.loadData();
            App.getScene().setRoot(root);
        } catch (IOException ex) {
            Logger.getLogger(MaintenancesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Brings user to the contractor view
     * @throws IOException
     */
    @FXML
    public void switchToContractors() throws IOException{
        App.setRoot("Contractors");
    }
}
