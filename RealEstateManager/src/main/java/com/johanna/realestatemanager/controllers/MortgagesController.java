package com.johanna.realestatemanager.controllers;

import com.johanna.realestatemanager.App;
import com.johanna.realestatemanager.models.DBManager;
import com.johanna.realestatemanager.models.FinancialInstitutionModel;
import com.johanna.realestatemanager.models.Mortgage;
import com.johanna.realestatemanager.models.MortgageModel;
import com.johanna.realestatemanager.models.PropertyModel;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Takes care of the mortgage view
 * @author Johanna
 */
public class MortgagesController extends AbstractMenu{
    
    Mortgage selectedMortgage;
    
    ObservableList<Mortgage> mortgage_data = FXCollections.observableArrayList();
    
    @FXML
    private TableView<Mortgage> table_mortgage;

    @FXML
    private TableColumn<Mortgage, String> col_address;

    @FXML
    private TableColumn<Mortgage, LocalDate> col_endDate;

    @FXML
    private TableColumn<Mortgage, String> col_fi;

    @FXML
    private TableColumn<Mortgage, Double> col_rate;

    @FXML
    private TableColumn<Mortgage, Double> col_monthPay;

    /**
     * Calls function when view is initialize
     */
    @FXML
    public void initialize() {
        initCol();
        loadTableData();
    }
    
    /**
     * Initialize column of the table
     */
    public void initCol(){
    	//Convert property id to address
        col_address.setCellValueFactory(cellData -> {
            String propertyId = cellData.getValue().getPropertyId();
            String address = PropertyModel.getPropertyAddress(propertyId);
            return new ReadOnlyStringWrapper(address);
            });
        col_endDate.setCellValueFactory(new PropertyValueFactory<>("endDate"));
        //Convert financial institutions id to name
        col_fi.setCellValueFactory(cellData -> {
            String fiId = cellData.getValue().getFiId();
            String fiName = FinancialInstitutionModel.getFiName(fiId);
            return new ReadOnlyStringWrapper(fiName);
            });
        col_rate.setCellValueFactory(new PropertyValueFactory<>("rate"));
        col_monthPay.setCellValueFactory(new PropertyValueFactory<>("monthPay"));
    }
    
    /**
     * Load data from database in the table
     */
    public void loadTableData(){
        table_mortgage.getItems().clear();
        for (Mortgage mortgage : MortgageModel.getMortgageList()){
            mortgage_data.add(mortgage);
        }
        table_mortgage.setItems(mortgage_data);
    }
    
    /**
     * Remove entry from database
     */
    @Override @FXML
    public void removeEntry() {
        Alert alert = showConfirmation("Are you sure you want to delete this mortgage?");
        
        Optional<ButtonType> option = alert.showAndWait();
        
        if (option.get() == ButtonType.OK) {
            selectedMortgage = table_mortgage.getSelectionModel().getSelectedItem();
            DBManager.removeEntry(selectedMortgage.getMortgageId(),"mortgage_id","mortgages");
            loadTableData();
        }
    }
    
    /**
     * Brings user to page to modify mortgages
     */
    @Override @FXML 
    public void modifyEntry(){
        selectedMortgage = table_mortgage.getSelectionModel().getSelectedItem();
        try {
        	//Pass mortgages selected to registration controller
            FXMLLoader loader = new FXMLLoader(App.class.getResource("RegisterMortgage.fxml"));
            Parent root = (Parent) loader.load();
            RegisterMortgageController controller = loader.getController();
            controller.setCurrentMortgage(selectedMortgage);
            controller.setNew(false);
            controller.loadData();
            App.getScene().setRoot(root);
        } catch (IOException ex) {
            Logger.getLogger(PropertiesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Leads user to registration page for new mortgages
     */
    @Override @FXML
    public void addEntry() throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("RegisterMortgage.fxml"));
        Parent root = (Parent) loader.load();
        RegisterMortgageController controller = loader.getController();
        controller.setNew(true);
        App.getScene().setRoot(root);
    }
    
    /**
     * Brings user to financial institution view
     * @throws IOException
     */
    @FXML
    private void switchToFI() throws IOException {
        App.setRoot("FinancialInstitutions");
    }
}
