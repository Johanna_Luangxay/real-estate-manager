package com.johanna.realestatemanager.controllers;

import com.johanna.realestatemanager.App;
import com.johanna.realestatemanager.models.Contractor;
import com.johanna.realestatemanager.models.ContractorModel;
import com.johanna.realestatemanager.models.Maintenance;
import com.johanna.realestatemanager.models.MaintenanceModel;
import com.johanna.realestatemanager.models.Property;
import com.johanna.realestatemanager.models.PropertyModel;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

/**
 * Takes care of the registration view for the maintenance
 * @author Johanna
 */
public class RegisterMaintenanceController extends AbstractInfo{

    private boolean isNew;
    
    private Maintenance currentMaintenance;
    
    @FXML
    private ChoiceBox input_contractor;

    @FXML
    private ComboBox input_address;

    @FXML
    private TextField input_cost;

    @FXML
    private DatePicker input_date;

    @FXML
    private TextField input_time;

    @FXML
    private TextArea input_reason;

    /**
     * Set boolean isNew
     * @param isNew boolean telling if the current maintenance is a new or a modification of already existing one
     */
    public  void setNew(boolean isNew) {
        this.isNew = isNew;
    }
    
    /**
     * Set the current maintenance
     * @param currentMaintenance gets a maintenance object
     */
    public void setCurrentMaintenance(Maintenance currentMaintenance){
        this.currentMaintenance = currentMaintenance;
    }
    
    /**
     * Is called when view is loaded. 
     */
    @FXML
    public void initialize(){
        initComboBox();
        initTextField();
    }
    
    /**
     * Initialize textfield to prevent user from writting undesired character
     */
    private void initTextField(){
        Validators.validateNumber(input_cost);
        Validators.validateTime(input_time);
    }
    
    /**
     * Initialize combobox with appropriate options
     */
    private void initComboBox(){
    	//Input for contractor
        ObservableList<String> contractorList = FXCollections.observableArrayList();
        ArrayList<Contractor> dbContractorList = ContractorModel.getContractorList();
        for(Contractor contractor : dbContractorList){
            contractorList.add(contractor.getFullName());
        }
        input_contractor.setItems(contractorList);
        
        //Input for property
        ObservableList<String> propertyList = FXCollections.observableArrayList();
        ArrayList<Property> dbPropertyList = PropertyModel.getPropertyList();
        for(Property property : dbPropertyList){
            propertyList.add(property.getAddress());
        }
        input_address.setItems(propertyList);       
    }
    
    /**
     * Display data in the input fields
     */
    public void loadData() {
        if (!(isNew))
        {
            input_contractor.setValue(ContractorModel.getContractorName(currentMaintenance.getContractorId()));
            input_address.setValue(PropertyModel.getPropertyAddress(currentMaintenance.getPropertyId()));
            input_cost.setText(Double.toString(currentMaintenance.getCost()));
            input_date.setValue(currentMaintenance.getDate());
            input_time.setText(currentMaintenance.getTime().toString());
            input_reason.setText(currentMaintenance.getReason());
        } 
    }

    /**
     * Confirms users wants to quit and leads them to the maintenance view
     */
    @Override
    public void exit() throws IOException{
        Alert alert = showConfirmation("Are you sure you want to exit? (Don't forget to save!)");
        
        Optional<ButtonType> option = alert.showAndWait();
      
        if (option.get() == ButtonType.OK) {
            App.setRoot("Maintenances");
        }
    }
    
    /**
     * Confirms user wants to save data and save them in database
     */
    @Override
    public void save() throws IOException{
        Alert alert = showConfirmation("Are you sure you want to change the informations?");
        
        Optional<ButtonType> option = alert.showAndWait();
        
        if (option.get() == ButtonType.OK) {
            if(isNew){
                addMaintenance();
                loadData();
            }
            else{
                modifyMaintenance(); 
                loadData();
            }
        }
    }
    
    /**
     * Update the current object with input from the user
     */
    private void updateData() throws Exception {
        //Id
        String maintenanceId;
        if(isNew){
            maintenanceId = MaintenanceModel.getLatestId();
        }
        else{
            maintenanceId = currentMaintenance.getMaintenanceId();
        }
        //Contractor
        String contractorId;
        try{
            contractorId = ContractorModel.getContractorId(input_contractor.getSelectionModel().getSelectedItem().toString());
        }
        catch(NullPointerException e){
            showError("You did not choose a contractor");
            throw new Exception("No contractor selected.");
        }
        //Property
        String propertyId;
        try{
            propertyId = PropertyModel.getPropertyId(input_address.getValue().toString());
        }
        catch(NullPointerException e){
            showError("You did not choose a property");
            throw new Exception("No property selected.");
        }
        //Cost
        double cost ;
        try{
            cost = Double.parseDouble(input_cost.getText());
        }
        catch(NumberFormatException e){
            cost = 0.00;
        }
        //Date
        LocalDate date = input_date.getValue();
        //Time
        LocalTime time = null;
        try{
            time = LocalTime.parse(input_time.getText());
        }
        catch(java.time.format.DateTimeParseException e){
            Alert alert = showConfirmation("You did not enter a time. Do you want to set the time to an hour from now?");
            Optional<ButtonType> option = alert.showAndWait();
        
            if (option.get() == ButtonType.OK) {
                time = LocalTime.parse(LocalTime.now().plusHours(1).format(DateTimeFormatter.ofPattern("HH:mm:ss")));         
            } 
            else {
                showWarning("Please set a time in the follwing format (HH:mm or HH:mm:ss)");               
                throw new Exception("Invalid input for time.");
            }
        }
        //Reason
        String reason = input_reason.getText();
        
        currentMaintenance = new Maintenance(maintenanceId, contractorId, propertyId, cost, date, time, reason);
        
        validateData();
    }
    
    /**
     * Validate user input
     */
    private void validateData() throws Exception{
        if(currentMaintenance.getDate() == null){
            Alert alert = showConfirmation("You did not enter a date. Do you want to put today's date?");
            Optional<ButtonType> option = alert.showAndWait();
        
            if (option.get() == ButtonType.OK) {
                currentMaintenance.setDate(LocalDate.now());         
            } 
            else {
                showWarning("Please set a date");               
                throw new Exception("No input for start date.");
            }
        }
    }
    
    /**
     * Calls model to add maintenance in database.
     */
    private void addMaintenance(){
        try{
           updateData();
            MaintenanceModel.addMaintenance(currentMaintenance);
            isNew = false;
            System.out.println("Maintenance added!"); 
        } 
        catch (Exception e) {
            System.out.println(e);
        } 
        
    }
    
    /**
     * Calls model to modify maintenance in database.
     */
    private void modifyMaintenance(){
        try{
            updateData();
            MaintenanceModel.modifyMaintenance(currentMaintenance);
        }
        catch (Exception e){
            System.out.println(e);
        }
        
    }

    
    
    
    
}
