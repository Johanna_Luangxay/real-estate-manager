/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.johanna.realestatemanager.controllers;

import com.johanna.realestatemanager.App;
import com.johanna.realestatemanager.models.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.fxml.FXML;
import javafx.scene.control.cell.*;
import javafx.scene.input.KeyCode;
import javafx.util.converter.DoubleStringConverter;

/**
 * Takes care of registration property view
 * @author Johanna
 */
public class RegisterPropertyController extends AbstractInfo {
        
    //private UtilitiesController utility = new UtilitiesController();
    
    private Property currentProperty;
    
    private boolean isNew;
        
    @FXML
    private TableView table_units;

    @FXML
    private TableColumn<Unit, String> col_unit;

    @FXML
    private TableColumn<Unit, String> col_tenant;

    @FXML
    private TableColumn<Unit, Double> col_gas;

    @FXML
    private TableColumn<Unit, Double> col_electricity;

    @FXML
    private TableColumn<CondoUnit, Double> col_condo;  
    
    
    @FXML
    private TextField input_address;

    @FXML
    private ComboBox input_type;
    
    @FXML
    private Spinner intSpinner;
    
    @FXML
    private TextArea input_restriction;

    @FXML
    private DatePicker input_ownership;

    @FXML
    private ToggleButton paid;

    @FXML
    private ToggleGroup payStatus;

    @FXML
    private ToggleButton unpaid;

    @FXML
    private TextField input_schoolTaxe;

    @FXML
    private TextField input_propertyTaxe;

    @FXML
    private TextField input_insurance;
    
    @FXML
    private Button showBtn;

    String type = "";
    
    /**
     * Is called when view is loaded to initialize some inputs fields
     */
    @FXML
    public void initialize(){
        initTextField();
        listenSpinner();
        initComboBox();
        initToggle();
        initTable();
    }
    
    /**
     * Initialize textfield to reject undesired characters.
     */
    private void initTextField(){
        Validators.validateAddress(input_address);
        Validators.validateNumber(input_propertyTaxe);
        Validators.validateNumber(input_insurance);
        Validators.validateNumber(input_schoolTaxe);
    }
    
    /**
     * When number of unit is chosen, updates the unit table
     */
    private void listenSpinner(){
        initSpinner();
        intSpinner.valueProperty().addListener((obs, oldValue, newValue) -> 
            addUnitRow((int)newValue)            
        );
    }
    
    /**
     * Initialize spinner for units
     */
    private void initSpinner(){
        int initialValue = 1; 
        if (currentProperty != null){
            initialValue = currentProperty.getUnit();
        } 
        if (type.equals("Condo") || type.equals("House")){
            intSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1,1, initialValue));
        }else{
            intSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1,999, initialValue));
        }
        //To not allow other non-digit value
        intSpinner.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.matches("\\d*") || newValue.matches("^0") || newValue.matches("\\d{4,}") ){
                intSpinner.getEditor().setText(oldValue);
            }
        });
    }
    
    /**
     * Display data of current property
     */
    public void loadData (){
        if (!(isNew))
        {
            input_address.setText(currentProperty.getAddress());
            input_type.setValue(currentProperty.getType());
            input_restriction.setText(currentProperty.getRestrictions());
            intSpinner.getValueFactory().setValue(currentProperty.getUnit());          
            input_ownership.setValue(currentProperty.getDateOfOwnership());
            setPayStatus(currentProperty.isIsPaid());
            input_schoolTaxe.setText(Double.toString(currentProperty.getSchoolTaxe()));
            input_propertyTaxe.setText(Double.toString(currentProperty.getPropertyTaxe()));
            input_insurance.setText(Double.toString(currentProperty.getInsurance()));    
            
            //Display data of unit table
            modifyUnitProperties();
            loadUnitTableData();            
        } 
    }
    
    /**
     * Display appropriate unit table depending on property type
     */
    public void modifyUnitProperties(){
        type = input_type.getSelectionModel().getSelectedItem().toString();
        initSpinner();
        table_units.getItems().clear();
        
        switch (type){
            case "Condo":
                table_units.getColumns().clear();
                table_units.getColumns().addAll(col_tenant, col_gas, col_electricity, col_condo);
                col_condo.setVisible(true);
                ObservableList<CondoUnit> condoData_table = FXCollections.observableArrayList();
                condoData_table.add(new CondoUnit("", 0 ,0, 0));
                table_units.setItems(condoData_table);
                break;
            case "House":
                table_units.getColumns().clear();
                table_units.getColumns().addAll(col_tenant, col_gas, col_electricity);
                ObservableList<HouseUnit> houseData_table = FXCollections.observableArrayList();
                houseData_table.add(new HouseUnit("", 0 ,0));
                table_units.setItems(houseData_table);
                break;
            case "Plexe":
                table_units.getColumns().clear();
                table_units.getColumns().addAll(col_unit, col_tenant, col_gas, col_electricity);
                ObservableList<PlexUnit> plexeData_table = FXCollections.observableArrayList();
                plexeData_table.add(new PlexUnit("1","", 0 ,0));
                table_units.setItems(plexeData_table);
                break;
            default:
                break;
        }
                
    }
    
    /**
     * Put data in the unit table
     */
    private void loadUnitTableData(){
        ObservableList<Unit> unit_data = FXCollections.observableArrayList();
        currentProperty.getPropertyUnit().forEach(curUnit -> {
            unit_data.add(curUnit);
        });
        table_units.setItems(unit_data);
    }
    
    /**
     * Set if the property is new
     * @param isNew boolean that says if view is used as add or modify/view
     */
    public void setNew(Boolean isNew){
        this.isNew = isNew;
    }
    
    /**
     * Set current property object
     * @param currentProperty property that is currently concerned
     */
    public void setCurrentProperty(Property currentProperty){
        this.currentProperty = currentProperty;
    }
    
    /**
     * Select the right toggle button depending on the status
     * @param status
     */
    private void setPayStatus(Boolean status){
        if (status){
            paid.setSelected(true);
        } else {
            unpaid.setSelected(true);
        }
    }
    
    /**
     * Initialize toggle button of payment status
     */
    private void initToggle(){
        paid.setUserData("true");
        unpaid.setUserData("false");
    }
    
    /**
     * Initialize comboBox for the different type of property
     */
    private void initComboBox(){
        ObservableList<String> typeList = FXCollections.observableArrayList("Condo","House","Plexe");
        input_type.setItems(typeList);
    }  
    
    /**
     * Initialize table unit
     */
    private void initTable(){
        initCols();
        table_units.setPlaceholder(new Label("No propriety type was selected."));
    }
    
    /**
     * Initialize column of unit table
     */
    private void initCols(){
        col_unit.setCellValueFactory(new PropertyValueFactory<>("unitId"));
        col_tenant.setCellValueFactory(new PropertyValueFactory<>("tenantName"));
        col_gas.setCellValueFactory(new PropertyValueFactory<>("gasFees"));
        col_electricity.setCellValueFactory(new PropertyValueFactory<>("electricityFees"));
        col_condo.setCellValueFactory(new PropertyValueFactory<>("condoFees"));
        
        editableCols();
    }
    
    /**
     * Make column of unit table editable
     */
    private void editableCols(){
        col_gas.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        col_electricity.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        col_condo.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        
        col_gas.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setGasFees(e.getNewValue());
        });
        col_electricity.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setElectricityFees(e.getNewValue());
        });
        col_condo.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setCondoFees(e.getNewValue());
        });
        
        table_units.setEditable(true);
        
        //To not allow other non-digit value
//        col_gas.textProperty().addListener((observable, oldValue, newValue) -> {
//            System.out.println("yes");
//            if(!newValue.matches("\\d*")){
//                col_gas.setText(oldValue);
//            }
//        });
        
    }
    
    /**
     * Confirms users wants to exit and leads them to property view
     */
    @Override
    public void exit() throws IOException{
        Alert alert = showConfirmation("Are you sure you want to exit? (Don't forget to save!)");
        
        Optional<ButtonType> option = alert.showAndWait();
      
        if (option.get() == ButtonType.OK) {
            App.setRoot("Properties");
        }   
    }
    
    /**
     * Confirms user wants to save and calls methods to proceed
     */
    @Override
    public void save() throws IOException, SQLException{
        Alert alert = showConfirmation("Are you sure you want to change the informations?");
        
        Optional<ButtonType> option = alert.showAndWait();
        
        if (option.get() == ButtonType.OK) {
            if(isNew){
                addProperty();
                loadData();
            }else{
                modifyProperty(); 
                loadData();
            }          
        }
    }
    
    /**
     * Update current property with user inputs
     */
    private void updateData() throws Exception{
        //Id
        String id;
        if(isNew){
            id = PropertyModel.getLatestId();
        }
        else{
            id = currentProperty.getId();
        }
        //Address
        String address = input_address.getText();
        try{
            type = input_type.getSelectionModel().getSelectedItem().toString();
        }
        catch(NullPointerException e){
            showError("You did not select the property type!");
            return;
        }
        //Unit
        int unit = (Integer) intSpinner.getValue();
        //Restrictions
        String restrictions = input_restriction.getText();
        //Date
        LocalDate dateOfOwnership = input_ownership.getValue();
        //Pay Status
        Boolean isPaid = null;
        try{
            isPaid = Boolean.parseBoolean((String) payStatus.getSelectedToggle().getUserData());
        }
        catch (NullPointerException e){
            showError("You did not select the payment status!");
            return;
        }
        //School Taxe
        double schoolTaxe;
        try{
            schoolTaxe = Double.parseDouble(input_schoolTaxe.getText());
        } 
        catch(NumberFormatException e){
            schoolTaxe = 0.0;
        }
        //Property Taxe
        double propertyTaxe;
        try{
            propertyTaxe = Double.parseDouble(input_propertyTaxe.getText());
        }
        catch(NumberFormatException e){
            propertyTaxe = 0.0;
        }
        //Insurance
        double insurance;
        try{
            insurance = Double.parseDouble(input_insurance.getText());
        }
        catch(NumberFormatException e){
            insurance = 0.0;
        }
        //Property Units
        ArrayList<Unit> propertyUnit = updateUnitData();
        
        currentProperty = new Property(id, address, type, unit, restrictions, 
            dateOfOwnership, isPaid, schoolTaxe, propertyTaxe, 
            insurance, propertyUnit);
        
        validateData();

    }
    
    /**
     * 
     * @throws Exception 
     */
    private void validateData() throws Exception{
        if(currentProperty.getAddress().isEmpty()){
            showError("You did not enter the property's address!");
            throw new Exception("No address was entered.");
        }
        if(currentProperty.getDateOfOwnership() == null){
            Alert alert = showConfirmation("You did not enter the date of ownership. Do you want to put today's date?");
            Optional<ButtonType> option = alert.showAndWait();
        
            if (option.get() == ButtonType.OK) {
                currentProperty.setDateOfOwnership(LocalDate.now());         
            } 
            else {
                showWarning("Please set the date of ownership");               
                throw new Exception("No input for date of ownership.");
            }
        }

    }
    
    /**
     * Returns array with unit object that correspond to user input 
     * @return ArrayList<unit> list of units input 
     */
    @FXML
    private ArrayList<Unit> updateUnitData(){
        
        ArrayList<Unit> unitList = new ArrayList();
        switch(type){
            case "Condo":
                Unit condoUnit = (CondoUnit) table_units.getItems().get(0);
                unitList.add(condoUnit);
                break;
            case "House":
                Unit houseUnit = (HouseUnit) table_units.getItems().get(0);
                unitList.add(houseUnit);
                break;
            case "Plexe":
                for (int i = 0; i<table_units.getItems().size();i++){
                    Unit plexeUnit = (PlexUnit) table_units.getItems().get(i);
                    unitList.add(plexeUnit);
                }
                break;
        }
        
        return unitList;
    }
    
    /**
     * Call models to add proeprty
     */
    private void addProperty(){
        try{
            updateData();
            PropertyModel.addProperty(currentProperty);
            isNew = false;
            System.out.println("Property added");
        }
        catch(Exception e){
            System.out.println(e.toString());
        }
    }
    
    /**
     * Call model to modify property
     */
    private void modifyProperty(){
        try{
            updateData();
            PropertyModel.modifyProperty(currentProperty);
        }
        catch(Exception e){
            System.out.println(e.toString());
        }
    }
    
    
    /**
     * Add row to table depending on number of unit is input in the spinner
     */
    public void addUnitRow(int newValue){
        
        int rowNum = table_units.getItems().size();        
        if(type.equals("Plexe")){
            ObservableList<PlexUnit> plexeData_table = FXCollections.observableArrayList();
            for(int i = 0; i < newValue; i++){
                plexeData_table.add(new PlexUnit(Integer.toString(i+1),"", 0 ,0));
            }
            table_units.setItems(plexeData_table);
        }

    }
    
    /**
     * Button to show column of tenants that live in which units
     */
    @FXML
    public void showTenants() {
        if(col_tenant.isVisible()) {
            col_tenant.setVisible(false);
            showBtn.setText("Show Tenants");
        } 
        else {
          col_tenant.setVisible(true);
          showBtn.setText("Hide Tenants");
        }
    }
    
}
