package com.johanna.realestatemanager.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

/**
 * Utility class for validating textfield
 * @author Johanna
 */
public class Validators {
    
	/**
	 * Make sure to allow only digits and a maximum of 10 digits
	 * @param input textfield to apply the rules
	 */
    public static void validatePhone(TextField input){
        ChangeListener<String> listener = (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("\\d*") || input.getText().length() > 10) {
                input.setText(newValue.replace(newValue, oldValue));
            }
        };
        
        input.textProperty().addListener(listener);
        
    }
    
    /**
     * Make sure to not allow not related character for a name
     * @param input textfield to apply the rules
     */
    public static void validateName(TextField input){
        ChangeListener<String> listener = (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("([a-zA-Z]*[ -']?[a-zA-Z]*)*")) {
                input.setText(newValue.replace(newValue, oldValue));
            }
        };
        
        input.textProperty().addListener(listener);
    }
    
    /**
     * Allow numbers an 1 dot only with 2 digits after period
     * @param input textfield to apply the rules
     */
    public static void validateNumber(TextField input){
        ChangeListener<String> listener = (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[0-9]*[.]?[0-9]{0,2}")) {
                input.setText(newValue.replace(newValue, oldValue));
            }
        };
        
        input.textProperty().addListener(listener);
    }
    
    /**
     * Allow character related to email address only
     * @param input textfield to apply the rules
     */
    public static void validateEmail(TextField input){
        ChangeListener<String> listener = (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("([a-z]*[._])*[@]?([a-z].?)*")) {
                input.setText(newValue.replace(newValue, oldValue));
            }
        };
        
        input.textProperty().addListener(listener);
    }
    
    /**
     * No character other than space, alphabets and digit allowed
     * @param input
     */
    public static void validateAddress(TextField input){
        ChangeListener<String> listener = (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[0-9]*([,]?[ ]?[a-zA-Z0-9]*)*")) {
                input.setText(newValue.replace(newValue, oldValue));
            }
        };
        
        input.textProperty().addListener(listener);
    }
    
    /**
     * Only : and digit are allowed
     * @param input textfield to apply the rules
     */
    public static void validateTime(TextField input){
        ChangeListener<String> listener = (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches("[0-9]{0,2}[:]?[0-9]{0,2}[:]?[0-9]{0,2}")) {
                input.setText(newValue.replace(newValue, oldValue));
            }
        };
            //"[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}"
        input.textProperty().addListener(listener);
    }
}
