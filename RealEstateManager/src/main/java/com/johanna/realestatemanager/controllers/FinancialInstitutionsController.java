package com.johanna.realestatemanager.controllers;

import com.johanna.realestatemanager.App;
import com.johanna.realestatemanager.models.DBManager;
import com.johanna.realestatemanager.models.FinancialInstitution;
import com.johanna.realestatemanager.models.FinancialInstitutionModel;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.DoubleStringConverter;

/**
 * Class that takes care of the financial institution view
 * @author Johanna
 */
public class FinancialInstitutionsController extends AbstractInfo{
    
    ObservableList<FinancialInstitution> fi_data = FXCollections.observableArrayList();
    
    @FXML
    private TableView<FinancialInstitution> table_fi;

    @FXML
    private TableColumn<FinancialInstitution, String> col_name;

    @FXML
    private TableColumn<FinancialInstitution, Double> col_interest;
    
    /**
     * Method that is called when view is called
     * @throws SQLException
     */
    @FXML
    public void initialize() throws SQLException{
        initCol();
        loadTableData();
    }
    
    /**
     * Method that initialize the column of the table
     */
    private void initCol(){
        col_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        col_interest.setCellValueFactory(new PropertyValueFactory<>("interest"));
        
        editableCols();
    }
    
    /**
     * Method that makes the columns editable
     */
    private void editableCols(){
        col_name.setCellFactory(TextFieldTableCell.forTableColumn());
        col_interest.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        
        col_name.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setName(e.getNewValue());
        });
        col_interest.setOnEditCommit(e->{
            e.getTableView().getItems().get(e.getTablePosition().getRow()).setInterest(e.getNewValue());
        });        
        
        table_fi.setEditable(true);
    }
    
    /**
     * Method that loads the data in the table from DB
     * @throws SQLException
     */
    private void loadTableData() throws SQLException{
        table_fi.getItems().clear();
        FinancialInstitutionModel.getFiList().forEach(fi -> {
            fi_data.add(fi);
        });
        table_fi.setItems(fi_data);
    }
    
    /**
     * Method to confirm exit and brings to mortgage page
     */
    @Override
    public void exit() throws IOException{
        Alert alert = showConfirmation("Are you sure you want to exit? (Don't forget to save!)");
        
        Optional<ButtonType> option = alert.showAndWait();
      
        if (option.get() == ButtonType.OK) {
            App.setRoot("Mortgages");
        }
    }
    
    /**
     * Method to save data in database
     */
    @Override
    public void save() throws IOException{
        Alert alert = showConfirmation("Are you sure you want to change the informations?");
        
        Optional<ButtonType> option = alert.showAndWait();
        
        if (option.get() == ButtonType.OK) {
            DBManager.deleteAll("financial_institutions");
            table_fi.getItems().forEach(fi -> {
                FinancialInstitutionModel.addFi(fi);
            });
        }
    }
    
    /**
     * Method to add a row to the table
     */
    @FXML
    public void addRow(){
        int rowNum = table_fi.getItems().size()+1;
        int latestId = FinancialInstitutionModel.getLatestId();
        int id = 0;
        if(rowNum > latestId){
            id = rowNum;
        }
        else if (rowNum == latestId){
            id = rowNum+1;
        }
        else {
            id = latestId;
        }
        table_fi.getItems().add(new FinancialInstitution(Integer.toString(id),"",0.0));
    }
    
    /**
     * Method to delete a row in the table
     */
    @FXML
    public void removeRow(){
        FinancialInstitution selectedFi = table_fi.getSelectionModel().getSelectedItem();
        table_fi.getItems().remove(selectedFi);
    }
    
    
}
