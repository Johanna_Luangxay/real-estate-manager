package com.johanna.realestatemanager.controllers;

import com.johanna.realestatemanager.App;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;

/**
 * Class from which all main page extends, it allows navigation bar to be functional.
 * @author Johanna
 */
public abstract class AbstractMenu {

	/**
	 * Method to remove entry
	 */
    @FXML
    public abstract void removeEntry();
    
    /**
     * Method to add entry
     * @throws IOException
     */
    @FXML
    public abstract void addEntry() throws IOException ;
    
    /**
     * Method to modify an entry
     */
    @FXML
    public abstract void modifyEntry();

    protected Alert showConfirmation(String message){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Are you sure?");
        alert.setHeaderText(message);
        
        return alert;
    }
    
    /**
     * Switch to property view
     * @throws IOException
     */
    @FXML
    private void switchToProperties() throws IOException {
        App.setRoot("Properties");
    }
    
    /**
     * Switch to tenants view
     * @throws IOException
     */
    @FXML
    private void switchToTenants() throws IOException {
        App.setRoot("Tenants");
    }
    
    /**
     * Switch to mortgage view    
     * @throws IOException
     */
    @FXML
    private void switchToMortgages() throws IOException {
        App.setRoot("Mortgages");
    }
    
    /**
     * Switch to maintenance view
     * @throws IOException
     */
    @FXML
    private void switchToMaintenances() throws IOException {
        App.setRoot("Maintenances");
    }
    
}
