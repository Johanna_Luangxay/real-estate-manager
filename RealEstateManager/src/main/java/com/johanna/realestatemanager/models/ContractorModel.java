/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.johanna.realestatemanager.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Johanna
 */
public class ContractorModel {
    
	/**
	 * Get next available id by calling dbmanager
	 * @return int
	 */
    public static int getLatestId(){
        int latestID = DBManager.getDBLatestId("contractor_id","contractors");
        return latestID;
    }
    
    /**
     * Return the name of the contractor based on its id
     * @param contractorId
     * @return String 
     */
    public static String getContractorName(String contractorId) {
        String query= "SELECT CONCAT(first_name, ' ', last_name) FROM contractors WHERE contractor_id LIKE ?";
        String name = DBManager.getString(query, contractorId);
        return name;
    }
    
    /**
     * Return the id of the contractor based on its full name
     * @param name
     * @return String
     */
    public static String getContractorId(String name) {
       String query = "SELECT contractor_id FROM contractors WHERE CONCAT(first_name, ' ', last_name) LIKE ?";
       String id = DBManager.getString(query, name);
       return id;
    }
    
    /**
     * Return arraylist of all the constractor
     * @return ArrayList<Contractor>
     */
    public static ArrayList<Contractor> getContractorList(){
        ArrayList<Contractor> contractorList = new ArrayList();
        try {
            DBManager db = new DBManager();
            String query = "SELECT * FROM contractors";
            PreparedStatement ps = db.conn.prepareStatement(query);
            ResultSet rps = ps.executeQuery();
            
            while(rps.next()) {
                String contractorId = rps.getString(1);
                String fname = rps.getString(2);
                String lname = rps.getString(3);
                String phone = rps.getString(4);
                String email = rps.getString(5);
                String type = rps.getString(6);
                
                Contractor currentContractor = new Contractor (contractorId,fname,lname, phone, email, type);
                contractorList.add(currentContractor);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ContractorModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return contractorList;
    }
    
    /**
     * Add a contractor in the database
     * @param newContractor Contractor object
     */
    public static void addContractor(Contractor newContractor){
        try {
            DBManager db = new DBManager();
            
            String insertStatement = "INSERT INTO contractors VALUES(?,?,?,?,?,?)";
            PreparedStatement ps =  db.conn.prepareStatement(insertStatement);
            
            ps.setString(1, newContractor.getContractorId());
            ps.setString(2, newContractor.getFname());
            ps.setString(3, newContractor.getLname());
            ps.setString(4, newContractor.getPhone());
            ps.setString(5, newContractor.getEmail());
            ps.setString(6, newContractor.getType());
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ContractorModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
