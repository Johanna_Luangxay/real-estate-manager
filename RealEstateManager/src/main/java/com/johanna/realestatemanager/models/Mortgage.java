package com.johanna.realestatemanager.models;

import java.time.LocalDate;

/**
 * Class object of a mortgage
 * @author Johanna
 */
public class Mortgage {
    private String mortgageId;
    private String propertyId;
    private double downPayment;
    private double value;
    private double amountLeft;
    private String fiId;
    private double rate;
    private LocalDate startDate;
    private LocalDate endDate;
    private double monthPay;

    /**
     * Constructor
     * @param mortgageId
     * @param propertyId
     * @param downPayment
     * @param value double value of the house
     * @param amountLeft double amount left to pay
     * @param fiId String financial institution id
     * @param rate double interest rate
     * @param startDate LocalDate start date of the contract
     * @param endDate LocalDate end date of the contract
     * @param monthPay double amount to pay per month
     */
    public Mortgage(String mortgageId, String propertyId, double downPayment, 
            double value, double amountLeft, String fiId, double rate, 
            LocalDate startDate, LocalDate endDate, double monthPay){
        
        this.mortgageId = mortgageId;
        this.propertyId = propertyId;
        this.downPayment = downPayment;
        this.value = value;
        this.amountLeft = amountLeft;
        this.fiId = fiId;
        this.rate = rate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.monthPay = monthPay;
        
    }
    
    /**
     * @return the mortgageId
     */
    public String getMortgageId() {
        return mortgageId;
    }

    /**
     * @return the propertyId
     */
    public String getPropertyId() {
        return propertyId;
    }

    /**
     * @param propertyId the propertyId to set
     */
    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    /**
     * @return the downPayment
     */
    public double getDownPayment() {
        return downPayment;
    }

    /**
     * @param downPayment the downPayment to set
     */
    public void setDownPayment(double downPayment) {
        this.downPayment = downPayment;
    }

    /**
     * @return the amountLeft
     */
    public double getAmountLeft() {
        return amountLeft;
    }

    /**
     * @param amountLeft the amountLeft to set
     */
    public void setAmountLeft(double amountLeft) {
        this.amountLeft = amountLeft;
    }

    /**
     * @return the fiId
     */
    public String getFiId() {
        return fiId;
    }

    /**
     * @param fiId the fiId to set
     */
    public void setFiId(String fiId) {
        this.fiId = fiId;
    }

    /**
     * @return the rate
     */
    public double getRate() {
        return rate;
    }

    /**
     * @param rate the rate to set
     */
    public void setRate(double rate) {
        this.rate = rate;
    }

    /**
     * @return the startDate
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public LocalDate getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the monthPay
     */
    public double getMonthPay() {
        return monthPay;
    }

    /**
     * @param monthPay the monthPay to set
     */
    public void setMonthPay(double monthPay) {
        this.monthPay = monthPay;
    }

    /**
     * @return the value
     */
    public double getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(double value) {
        this.value = value;
    }
}
