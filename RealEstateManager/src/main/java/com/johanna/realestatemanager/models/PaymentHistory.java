package com.johanna.realestatemanager.models;

import java.time.LocalDate;

/**
 * Class object of a payment
 * @author Johanna
 */
public class PaymentHistory {
    private String paymentId;
    private String tenantId;
    private LocalDate payDate;
    private String method;
    private double amount;

    /**
     * Constructor
     * @param paymentId
     * @param tenantId
     * @param payDate
     * @param method
     * @param amount
     */
    public PaymentHistory(String paymentId, String tenantId, 
            LocalDate payDate, String method, double amount){
        this.paymentId = paymentId;
        this.tenantId = tenantId;
        this.payDate = payDate;
        this.method = method;
        this.amount = amount;
    }

    /**
     * @return the paymentId
     */
    public String getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the tenantId
     */
    public String getTenantId() {
        return tenantId;
    }

    /**
     * @param tenantId the tenantId to set
     */
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * @return the payDate
     */
    public LocalDate getPayDate() {
        return payDate;
    }

    /**
     * @param payDate the payDate to set
     */
    public void setPayDate(LocalDate payDate) {
        this.payDate = payDate;
    }

    /**
     * @return the method
     */
    public String getMethod() {
        return method;
    }

    /**
     * @param method the method to set
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

}
