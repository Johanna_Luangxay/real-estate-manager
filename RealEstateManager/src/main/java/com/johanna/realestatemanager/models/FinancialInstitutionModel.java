package com.johanna.realestatemanager.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class Model that takes care of financial institution data
 * @author Johanna
 */
public class FinancialInstitutionModel {
    
	/**
	 * Latest id available for financial institution table
	 * @return
	 */
    public static int getLatestId(){
        int latestID = DBManager.getDBLatestId("payment_id","payment_history");
        return latestID;
    }
    
    /**
     * Return arrayLIst of financial institution that are in the database
     * @return
     */
    public static ArrayList<FinancialInstitution> getFiList(){
        ArrayList<FinancialInstitution> fiList = new ArrayList();
        try {
            DBManager db = new DBManager();
            String query = "SELECT * FROM financial_institutions";
            PreparedStatement ps = db.conn.prepareStatement(query);
            ResultSet rps = ps.executeQuery();
            
            while(rps.next()) {
                String fiId = rps.getString(1);
                String fiName = rps.getString(2);
                double interest = rps.getDouble(3);
                
                FinancialInstitution currentFi = new FinancialInstitution(fiId,fiName,interest);
                fiList.add(currentFi);
            }
        } 
        catch (SQLException ex) {
            Logger.getLogger(FinancialInstitutionModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return fiList;
    }
    
    /**
     * Get the id of a financial institution name
     * @param fiName
     * @return String
     */
    public static String getFiId(String fiName){
        String query = "SELECT fi_id FROM financial_institutions WHERE name LIKE ?";
        String fiId = DBManager.getString(query, fiName);
        return fiId;
    }
    
    /**
     * Return name of financial institution based on its id
     * @param fiId
     * @return String
     */
    public static String getFiName(String fiId){
        String query = "SELECT name FROM financial_institutions WHERE fi_id LIKE ?";
        String fiName = DBManager.getString(query, fiId);
        return fiName;
    }
    
    /**
     * Add a financial institution in the database
     * @param newFi
     */
    public static void addFi(FinancialInstitution newFi){
        try {
            DBManager db = new DBManager();
            
            String insertStatement = "INSERT INTO financial_institutions VALUES(?,?,?)";
            PreparedStatement ps =  db.conn.prepareStatement(insertStatement);
            
            ps.setString(1, newFi.getFiId());
            ps.setString(2, newFi.getName());
            ps.setDouble(3, newFi.getInterest());

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FinancialInstitutionModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
