package com.johanna.realestatemanager.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class models that takes care of data of tenants
 * @author Johanna
 */
public class TenantModel {
    
	/**
	 * Gives the fullname of the tenant based on its id
	 * @param tenantId
	 * @return
	 */
    public static String getTenantName(String tenantId){
        String query = "SELECT CONCAT(first_name, ' ', last_name) FROM tenants WHERE tenant_id LIKE ?";
        String tenantName = DBManager.getString(query, tenantId);
        return tenantName;
        
    }
    
    /**
     * Gives a tenant id based on its fullname
     * @param tenantName
     * @return
     */
    public static String getTenantId(String tenantName){
        String query = "SELECT tenant_id FROM tenants WHERE CONCAT(first_name, ' ', last_name) LIKE ?";
        String tenantId = DBManager.getString(query, tenantName);
        return tenantId;
    }
    
    /**
     * Gives latest id available for tenant
     * @return
     */
    public static String getLatestId(){
        int latestID = DBManager.getDBLatestId("tenant_id","tenants")+1;
        return Integer.toString(latestID);
    }
    
    /**
     * Gives an arraylist of all tenants in the database
     * @return
     */
    public static ArrayList<Tenant> getTenantList() {
        ArrayList<Tenant> tenantList = new ArrayList();
        try {            
            DBManager db = new DBManager();
            String query = "SELECT * FROM tenants";
            PreparedStatement ps = db.conn.prepareStatement(query);
            ResultSet rps = ps.executeQuery();
            
            while(rps.next()) {
                String tenantId = rps.getString(1);
                String fname = rps.getString(2);
                String lname = rps.getString(3);
                String propertyId = rps.getString(4);
                String unitId = rps.getString(5);
                String email = rps.getString(6);
                String phone = rps.getString(7);
                double rent = rps.getDouble(8);
                double rate = rps.getDouble(9);
                
                Tenant currentTenant = new Tenant(tenantId,fname,lname,propertyId,unitId,email,phone,rent,rate);
                tenantList.add(currentTenant);
            }
        } 
        catch (SQLException ex) {
            Logger.getLogger(TenantModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tenantList;
    }
    
    /**
     * Gets the name of the tenant living in the unit
     * @param property
     * @param unit
     * @return
     */
    public static String getLivingTenant(String property, String unit){
        String tenant = null;
            try {
                DBManager db = new DBManager();
                String query = "SELECT tenant_id FROM tenants WHERE property_id LIKE ? AND unit_id LIKE ?";
                PreparedStatement ps = db.conn.prepareStatement(query);
                ps.setString(1,property);
                ps.setString(2, unit);
                ResultSet rps = ps.executeQuery(); 
                if(rps.next()){
                   tenant = rps.getString(1); 
                }
            } catch (SQLException ex) { 
            Logger.getLogger(PropertyModel.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return tenant;     
    }
    
    /**
     * Add tenant in the database
     * @param newTenant
     */
    public static void addTenant(Tenant newTenant){
        try {
            DBManager db = new DBManager();
            
            String insertTenantStatement = "INSERT INTO tenants VALUES(?,?,?,?,?,?,?,?,?)";
            PreparedStatement pts =  db.conn.prepareStatement(insertTenantStatement);
            
            pts.setString(1, newTenant.getTenantId());
            pts.setString(2, newTenant.getFirstName());
            pts.setString(3, newTenant.getLastName());
            pts.setString(4, newTenant.getPropertyId());
            pts.setString(5, newTenant.getUnitId());
            pts.setString(6, newTenant.getEmail());
            pts.setString(7, newTenant.getPhone());
            pts.setDouble(8, newTenant.getRent());
            pts.setDouble(9, newTenant.getRate());

            pts.executeUpdate();
            
            PropertyModel.updatePropertyUnit(newTenant.getPropertyId(),newTenant.getUnitId(), newTenant.getTenantId());
            
        } catch (SQLException ex) {
            Logger.getLogger(TenantModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Modify tenant in the database
     * @param tenant
     */
    public static void modifyTenant(Tenant tenant){
        try {
            DBManager db = new DBManager();
            
            String updateStatement = "UPDATE tenants SET "
                    + "first_name = ?,"
                    + "last_name = ?,"
                    + "property_id = ?,"
                    + "unit_id = ?,"
                    + "email = ?,"
                    + "phone = ?,"
                    + "monthly_rent = ?,"
                    + "rent_rate = ?"
                    + "WHERE tenant_id LIKE ?";
            
            PreparedStatement ps =  db.conn.prepareStatement(updateStatement);
            
            ps.setString(1, tenant.getFirstName());
            ps.setString(2, tenant.getLastName());
            ps.setString(3, tenant.getPropertyId());
            ps.setString(4,tenant.getUnitId());
            ps.setString(5,tenant.getEmail());
            ps.setString(6,tenant.getPhone());
            ps.setDouble(7,tenant.getRent());
            ps.setDouble(8,tenant.getRate());
            ps.setString(9, tenant.getTenantId());
            
            
            PropertyModel.updatePropertyUnit(tenant.getPropertyId(),tenant.getUnitId(), tenant.getTenantId());
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TenantModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 

}
