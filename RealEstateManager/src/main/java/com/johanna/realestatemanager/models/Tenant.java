package com.johanna.realestatemanager.models;

/**
 * Class of tenant object
 * @author Johanna
 */
public class Tenant {
    
    private String tenantId;
    private String firstName;
    private String lastName;
    private String propertyId;
    private String unitId;
    private String email;
    private String phone;
    private double rent;
    private double rate;
    
    /**
     * Constructor
     * @param tenantId
     * @param firstName
     * @param lastName
     * @param propertyId
     * @param unitId
     * @param email
     * @param phone
     * @param rent
     * @param rate
     */
    public Tenant(String tenantId,String firstName,String lastName,String propertyId,
            String unitId,String email,String phone,double rent,double rate){
        this.tenantId = tenantId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.propertyId = propertyId;
        this.unitId = unitId;
        this.email = email;
        this.phone = phone;
        this.rent = rent;
        this.rate = rate;
    }
    
    /**
     * @return the tenantId
     */
    public String getTenantId() {
        return tenantId;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the propertyId
     */
    public String getPropertyId() {
        return propertyId;
    }

    /**
     * @param propertyId the propertyId to set
     */
    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    /**
     * @return the unitId
     */
    public String getUnitId() {
        return unitId;
    }

    /**
     * @param unitId the unitId to set
     */
    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the rent
     */
    public double getRent() {
        return rent;
    }

    /**
     * @param rent the rent to set
     */
    public void setRent(double rent) {
        this.rent = rent;
    }
    
    /**
     * @return the rate
     */
    public double getRate() {
        return rate;
    }

    /**
     * @param rate the rate to set
     */
    public void setRate(double rate) {
        this.rate = rate;
    }
    
//    
}
