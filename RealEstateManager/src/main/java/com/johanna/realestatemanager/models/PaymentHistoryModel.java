package com.johanna.realestatemanager.models;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Johanna
 */
public class PaymentHistoryModel {
    
	/**
	 * Return next available id for payment
	 * @return int
	 */
    public static int getLatestId(){
        int latestID = DBManager.getDBLatestId("payment_id","payment_history");
        return latestID;
    }
    
    /**
     * Return arraylist of all the payment in the database
     * @return ArrayList<PaymentHistory>
     */
    public static ArrayList<PaymentHistory> getPaymentList(){
        
        ArrayList<PaymentHistory> paymentList = new ArrayList();
        
        try {
            DBManager db = new DBManager();
            String query = "SELECT * FROM payment_history";
            PreparedStatement ps = db.conn.prepareStatement(query);
            ResultSet rps = ps.executeQuery();
            
            while(rps.next()) {
                String paymentId = rps.getString(1);
                String tenantId = rps.getString(2);
                LocalDate payDate = rps.getDate(3).toLocalDate();
                String method = rps.getString(4);
                double amount = rps.getDouble(5);
                
                PaymentHistory currentPayment = new PaymentHistory(paymentId,tenantId,payDate,method,amount);
                paymentList.add(currentPayment);
            }
            
        } 
        catch (SQLException ex) {
            Logger.getLogger(PaymentHistoryModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return paymentList;
    }
    
    /**
     * Add a payment in the database
     * @param newPayment
     */
    public static void addPayment(PaymentHistory newPayment){
        try {
            DBManager db = new DBManager();
            
            String insertStatement = "INSERT INTO payment_history VALUES(?,?,?,?,?)";
            PreparedStatement ps =  db.conn.prepareStatement(insertStatement);
            
            ps.setString(1, newPayment.getPaymentId());
            ps.setString(2, newPayment.getTenantId());
            ps.setDate(3, java.sql.Date.valueOf(newPayment.getPayDate()));
            ps.setString(4, newPayment.getMethod());
            ps.setDouble(5, newPayment.getAmount());

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PaymentHistoryModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
