package com.johanna.realestatemanager.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class model that takes care of data of maintenance
 * @author Johanna
 */
public class MaintenanceModel {

	/**
	 * Return latest id available for maintenance
	 * @return String
	 */
    public static String getLatestId() {
        int latestID = DBManager.getDBLatestId("maintenance_id","maintenances")+1;
        return Integer.toString(latestID);
    }
    
    /**
     * Return an array list of maintenance containing all maintenance existing
     * @return ArrayList<Maintenance>
     */
    public static ArrayList<Maintenance> getMaintenanceList() {
        ArrayList<Maintenance> maintenanceList = new ArrayList();
        try {
            DBManager db = new DBManager();
            String query = "SELECT * FROM maintenances";
            PreparedStatement ps = db.conn.prepareStatement(query);
            ResultSet rps = ps.executeQuery();
            
            while(rps.next()) {
                String maintenanceId = rps.getString(1);
                String contractorId = rps.getString(2);
                String propertyId = rps.getString(3);
                double cost = rps.getDouble(4);
                LocalDate date = rps.getDate(5).toLocalDate();
                LocalTime time = rps.getTime(6).toLocalTime();
                String reason = rps.getString(7);
                
                Maintenance currentMaintenance = new Maintenance (maintenanceId, 
                        contractorId, propertyId, cost, date, time, reason);
                maintenanceList.add(currentMaintenance);
            } 
        }
        catch (SQLException ex) {
            Logger.getLogger(MaintenanceModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return maintenanceList;
    }
    
    /**
     * Add a maintenance in the database
     * @param newMaintenance
     */
    public static void addMaintenance(Maintenance newMaintenance) {
        try {
            DBManager db = new DBManager();
            
            String insertStatement = "INSERT INTO maintenances VALUES(?,?,?,?,?,?,?)";
            PreparedStatement ps =  db.conn.prepareStatement(insertStatement);
            
            ps.setString(1, newMaintenance.getMaintenanceId());
            ps.setString(2, newMaintenance.getContractorId());
            ps.setString(3, newMaintenance.getPropertyId());
            ps.setDouble(4, newMaintenance.getCost());
            ps.setDate(5, java.sql.Date.valueOf(newMaintenance.getDate()));
            ps.setTime(6, java.sql.Time.valueOf(newMaintenance.getTime()));
            ps.setString(7, newMaintenance.getReason());
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MaintenanceModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Modify a row in the maintenance table
     * @param maintenance
     */
    public static void modifyMaintenance(Maintenance maintenance) {
        try {
            DBManager db = new DBManager();
            
            String updateStatement = "UPDATE maintenances SET "
                    + "contractor_id = ?,"
                    + "property_id = ?,"
                    + "cost = ?,"
                    + "appointment_date = ?,"
                    + "appointment_time = ?,"
                    + "reason = ? "
                    + "WHERE maintenance_id LIKE ?";
            
            PreparedStatement ps =  db.conn.prepareStatement(updateStatement);
            
            ps.setString(1, maintenance.getContractorId());
            ps.setString(2, maintenance.getPropertyId());
            ps.setDouble(3, maintenance.getCost());
            ps.setDate(4, java.sql.Date.valueOf(maintenance.getDate()));
            ps.setTime(5, java.sql.Time.valueOf(maintenance.getTime()));
            ps.setString(6, maintenance.getReason());
            ps.setString(7, maintenance.getMaintenanceId());
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MaintenanceModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
