package com.johanna.realestatemanager.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class model that takes care of mortgage data in database
 * @author Johanna
 */
public class MortgageModel {
    
	/**
	 * Return latest id available for mortgage
	 * @return Sting
	 */
    public static String getLatestId(){
        int latestID = DBManager.getDBLatestId("mortgage_id","mortgages")+1;
        return Integer.toString(latestID);
    }
    
    /**
     * Return arraylist of all mortgage in database 
     * @return ArrayList<Mortgage>
     */
    public static ArrayList<Mortgage> getMortgageList() {
        ArrayList<Mortgage> mortgageList = new ArrayList();
        try {
            DBManager db = new DBManager();
            String query = "SELECT * FROM mortgages";
            PreparedStatement ps = db.conn.prepareStatement(query);
            ResultSet rps = ps.executeQuery();
            
            while(rps.next()) {
                String mortgageId = rps.getString(1);
                String propertyId = rps.getString(2);
                double downPay = rps.getDouble(3);
                double value = rps.getDouble(4);
                double amountLeft = rps.getDouble(5);
                String fiId = rps.getString(6);
                double rate = rps.getDouble(7);
                LocalDate startDate = rps.getDate(8).toLocalDate();
                LocalDate endDate = rps.getDate(9).toLocalDate();
                double monthPay = rps.getDouble(10);
                
                Mortgage currentMortgage = new Mortgage(mortgageId, propertyId, downPay, 
                        value, amountLeft,fiId, rate, startDate, endDate, monthPay);
                mortgageList.add(currentMortgage);
            }  
        } 
        catch (SQLException ex) {
            Logger.getLogger(MortgageModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mortgageList;
    }
    
    /**
     * Add a mortgage to database
     * @param newMortgage
     */
    public static void addMortgage(Mortgage newMortgage) {
        try {
            DBManager db = new DBManager();
            
            String insertStatement = "INSERT INTO mortgages VALUES(?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps =  db.conn.prepareStatement(insertStatement);
            
            ps.setString(1, newMortgage.getMortgageId());
            ps.setString(2, newMortgage.getPropertyId());
            ps.setDouble(3, newMortgage.getDownPayment());
            ps.setDouble(4, newMortgage.getValue());
            ps.setDouble(5, newMortgage.getAmountLeft());
            ps.setString(6, newMortgage.getFiId());
            ps.setDouble(7, newMortgage.getRate());
            ps.setDate(8, java.sql.Date.valueOf(newMortgage.getStartDate()));
            ps.setDate(9, java.sql.Date.valueOf(newMortgage.getEndDate()));
            ps.setDouble(10, newMortgage.getMonthPay());

            ps.executeUpdate();
            
        } 
        catch (SQLException ex) {
             Logger.getLogger(MortgageModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Modify an existing mortgage in the database
     * @param mortgage
     */
    public static void modifyMortgage(Mortgage mortgage) {
        try {
            DBManager db = new DBManager();
            
            String updateStatement = "UPDATE mortgages SET "
                    + "property_id = ?,"
                    + "down_payment = ?,"
                    + "property_value = ?,"
                    + "amount_left = ?,"
                    + "fi_id = ?,"
                    + "interest_rate = ?,"
                    + "start_mortgage = ?,"
                    + "end_mortgage = ?, "
                    + "monthly_payment = ? "
                    + "WHERE mortgage_id LIKE ?";
            
            PreparedStatement ps =  db.conn.prepareStatement(updateStatement);
            
            ps.setString(1, mortgage.getPropertyId());
            ps.setDouble(2, mortgage.getDownPayment());
            ps.setDouble(3, mortgage.getValue());
            ps.setDouble(4, mortgage.getAmountLeft());
            ps.setString(5, mortgage.getFiId());
            ps.setDouble(6, mortgage.getRate());
            ps.setDate(7, java.sql.Date.valueOf(mortgage.getStartDate()));
            ps.setDate(8, java.sql.Date.valueOf(mortgage.getEndDate()));
            ps.setDouble(9, mortgage.getMonthPay());
            ps.setString(10, mortgage.getMortgageId());
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MortgageModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
