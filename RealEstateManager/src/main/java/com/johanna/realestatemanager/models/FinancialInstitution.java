/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.johanna.realestatemanager.models;

/**
 * Financial Institution object
 * @author Johanna
 */
public class FinancialInstitution {
    private String fiId;
    private String name;
    private double interest;
    
    /**
     * Constructor
     * @param fiId financial institution id
     * @param name
     * @param interest
     */
    public FinancialInstitution(String fiId, String name, double interest){
        this.fiId = fiId;
        this.name = name;
        this.interest = interest;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the interest
     */
    public double getInterest() {
        return interest;
    }

    /**
     * @param interest the interest to set
     */
    public void setInterest(double interest) {
        this.interest = interest;
    }

    /**
     * @return the fiId
     */
    public String getFiId() {
        return fiId;
    }

    /**
     * @param fiId the fiId to set
     */
    public void setFiId(String fiId) {
        this.fiId = fiId;
    }
}
