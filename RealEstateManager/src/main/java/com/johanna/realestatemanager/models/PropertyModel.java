package com.johanna.realestatemanager.models;

import com.johanna.realestatemanager.controllers.RegisterPropertyController;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class model that takes care of data concerning property
 * @author Johanna
 */
public class PropertyModel {
    
	/**
	 * Return next id available for property
	 * @return
	 */
    public static String getLatestId(){
        int latestID = DBManager.getDBLatestId("property_id","properties") +1;
        return Integer.toString(latestID);
    }
    
    /**
     * Return id of a property based on its address
     * @param address
     * @return
     */
    public static String getPropertyId(String address){
        String query = "SELECT property_id FROM properties WHERE address LIKE ?";
        String propertyId = DBManager.getString(query,address);
        return propertyId;
    }
    
    /**
     * Return address of a property based on its id
     * @param propertyId
     * @return
     */
    public static String getPropertyAddress(String propertyId){
        String query = "SELECT address FROM properties WHERE property_id LIKE ?";
        String address = DBManager.getString(query, propertyId);
        return address;
    }
    
    /**
     * Returns list of unit that a property has
     * @param propertyId
     * @return
     */
    public static ArrayList<String> getUnitList(String propertyId){
        ArrayList<String> unitList = new ArrayList();
        try {
            DBManager db = new DBManager();
            String query = "SELECT unit_id FROM units WHERE property_id LIKE ?";
            PreparedStatement ps = db.conn.prepareStatement(query);
            ps.setString(1, propertyId);
            ResultSet rps = ps.executeQuery();
            
            while(rps.next()) {
                unitList.add(rps.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PropertyModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return unitList;
    }
    
    /**
     * Add a property in database as well as its units
     * @param newProperty
     */
    public static void addProperty(Property newProperty){
        
        try {
            DBManager db = new DBManager();
            
            String insertPropertyStatement = "INSERT INTO properties VALUES(?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pps =  db.conn.prepareStatement(insertPropertyStatement);
            
            pps.setString(1, newProperty.getId());
            pps.setString(2, newProperty.getAddress());
            pps.setString(3, newProperty.getType());
            pps.setInt(4, newProperty.getUnit());
            pps.setString(5, newProperty.getRestrictions());
            pps.setDate(6, java.sql.Date.valueOf(newProperty.getDateOfOwnership()));
            pps.setBoolean(7, newProperty.isIsPaid());
            pps.setDouble(8, newProperty.getSchoolTaxe());
            pps.setDouble(9, newProperty.getPropertyTaxe());
            pps.setDouble(10,newProperty.getInsurance());
            
            pps.executeUpdate();
            
            // Add the units
            for(int i = 0 ; i < newProperty.getPropertyUnit().size() ; i++){
                String insertUnitStatement = "INSERT INTO units VALUES(?,?,?,?,?,?)";
                PreparedStatement pus = db.conn.prepareStatement(insertUnitStatement);
            
                pus.setString(1,newProperty.getPropertyUnit().get(i).getUnitId());
                pus.setString(2,newProperty.getId());
                pus.setString(3,null);
                pus.setDouble(4,newProperty.getPropertyUnit().get(i).getGasFees());
                pus.setDouble(5,newProperty.getPropertyUnit().get(i).getElectricityFees());
                
                if (newProperty.getType().equals("Condo")){
                    pus.setDouble(6,((CondoUnit)newProperty.getPropertyUnit().get(i)).getCondoFees());
                } else {
                    pus.setDouble(6, 0);
                }
                
                pus.executeUpdate();
            }
             
        } catch (SQLException ex) {
            Logger.getLogger(RegisterPropertyController.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }

    /**
     * Modify an existing property in database
     * @param newProperty
     */
    public static void modifyProperty(Property newProperty){       
        try {
            DBManager db = new DBManager();
            Statement s =  db.conn.createStatement();
            s.executeUpdate("SET FOREIGN_KEY_CHECKS = 0"); 
            
            String insertStatement = "UPDATE properties SET "
                    + "address = ?,"
                    + "type = ?,"
                    + "unit = ?,"
                    + "restrictions = ?,"
                    + "date_ownership = ?,"
                    + "ispaid = ?,"
                    + "school_taxe = ?,"
                    + "property_taxe = ?,"
                    + "insurance = ? "
                    + "WHERE property_id LIKE ?";
            
            PreparedStatement ps =  db.conn.prepareStatement(insertStatement);
            
            ps.setString(1, newProperty.getAddress());
            ps.setString(2, newProperty.getType());
            ps.setInt(3, newProperty.getUnit());
            ps.setString(4, newProperty.getRestrictions());
            ps.setDate(5, java.sql.Date.valueOf(newProperty.getDateOfOwnership()));
            ps.setBoolean(6, newProperty.isIsPaid());
            ps.setDouble(7, newProperty.getSchoolTaxe());
            ps.setDouble(8, newProperty.getPropertyTaxe());
            ps.setDouble(9,newProperty.getInsurance());
            ps.setString(10,newProperty.getId());
            
            ps.executeUpdate();
            //Remove units of the property
            DBManager.removeEntry(newProperty.getId(),"property_id","units");
            //Add units of the property
            for(int i = 0 ; i < newProperty.getPropertyUnit().size() ; i++){
                String insertUnitStatement = "INSERT INTO units VALUES(?,?,?,?,?,?)";
                PreparedStatement pus = db.conn.prepareStatement(insertUnitStatement);
            
                String unitId = newProperty.getPropertyUnit().get(i).getUnitId();
                pus.setString(1,unitId);
                pus.setString(2,newProperty.getId());
                pus.setString(3,TenantModel.getLivingTenant(newProperty.getId(), unitId));
                pus.setDouble(4,newProperty.getPropertyUnit().get(i).getGasFees());
                pus.setDouble(5,newProperty.getPropertyUnit().get(i).getElectricityFees());
                
                if (newProperty.getType().equals("Condo")){
                    pus.setDouble(6,((CondoUnit)newProperty.getPropertyUnit().get(i)).getCondoFees());
                } else {
                    pus.setDouble(6, 0);
                }
                
                pus.executeUpdate();
            }
            
            s.executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
        } 
        
        catch (SQLException ex) {
            Logger.getLogger(RegisterPropertyController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Update a unit with a tenant
     * @param property 
     * @param unit
     * @param tenant
     */
    public static void updatePropertyUnit (String property, String unit, String tenant){
        try {
            DBManager db = new DBManager();
            Statement s =  db.conn.createStatement(); 
            s.executeUpdate("SET FOREIGN_KEY_CHECKS = 0");
            
            removeOldLocation(tenant);
            
            String insertStatement = "UPDATE units SET tenant_id = ? WHERE property_id = ? and unit_id = ?";
            PreparedStatement ps =  db.conn.prepareStatement(insertStatement);
            
            ps.setString(1, tenant);
            ps.setString(2, property);
            ps.setString(3, unit);    
            
            ps.executeUpdate();
            
            s.executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
            
        } catch (SQLException ex) {
            Logger.getLogger(PropertyModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /** 
     * Remove a tenant from unit
     * @param tenant
     * @throws SQLException
     */
    private static void removeOldLocation(String tenant) throws SQLException{
        DBManager db = new DBManager();
        
        String removeStatement = "UPDATE units SET tenant_id = null WHERE property_id = ? and unit_id = ?";
        PreparedStatement ps =  db.conn.prepareStatement(removeStatement);
        
        String propertyQuery = "SELECT property_id FROM units WHERE tenant_id LIKE ?";
        String propertyId = DBManager.getString(propertyQuery, tenant);
        ps.setString(1, propertyId);
            
        String unitQuery = "SELECT unit_id FROM units WHERE tenant_id LIKE ?";
        String unitId = DBManager.getString(unitQuery, tenant);
        ps.setString(2, unitId);
           
        ps.executeUpdate();
    }
    
    /**
     * Return list of all property from database
     * @return the propertyList
     */
    public static ArrayList<Property> getPropertyList() {
        ArrayList<Property> propertyList = new ArrayList();
        try {
            
            DBManager db = new DBManager();
            String query = "SELECT * FROM properties";
            PreparedStatement ps = db.conn.prepareStatement(query);
            ResultSet rps = ps.executeQuery();
            
            while(rps.next()) {
                String propertyId = rps.getString(1);
                String address = rps.getString(2);
                String type = rps.getString(3);
                int unit = rps.getInt(4);
                String restrictions = rps.getString(5);
                LocalDate dateOfOwnership = rps.getDate(6).toLocalDate();
                Boolean isPaid = rps.getBoolean(7);
                double schoolTaxe = rps.getDouble(8);
                double propertyTaxe = rps.getDouble(9);
                double insurance = rps.getDouble(10);
                
                ArrayList<Unit> propertyUnitList = new ArrayList();
                
                String unitQuery = "SELECT unit_id, tenant_id, gas, electricity, condoFees FROM units WHERE property_id LIKE ?";
                PreparedStatement ups = db.conn.prepareStatement(unitQuery);
                ups.setString(1, propertyId);
                ResultSet rups = ups.executeQuery();
                
                //Get unit of the property
                while(rups.next()){
                    String unitId = rups.getString(1);
                    String tenantId = rups.getString(2);
                    double gasFees = rups.getDouble(3);
                    double electricityFees = rups.getDouble(4);
                    double condoFees = rups.getDouble(5);
                    
                    String tenantName = TenantModel.getTenantName(tenantId);
                    
                    Unit propertyUnit;
                    
                    switch (type){
                        case "Condo":
                            propertyUnit = new CondoUnit(tenantName, gasFees, electricityFees, condoFees);
                            propertyUnitList.add(propertyUnit);
                            break;
                        case "House":
                            propertyUnit = new HouseUnit(tenantName,gasFees,electricityFees);
                            propertyUnitList.add(propertyUnit);
                            break;
                        case "Plexe":
                            propertyUnit = new PlexUnit(unitId, tenantName, gasFees, electricityFees);
                            propertyUnitList.add(propertyUnit);
                            break;
                    }
                }
                
                Property currentProperty = new Property(propertyId, address, type, unit, restrictions,
                        dateOfOwnership, isPaid, schoolTaxe, propertyTaxe,
                        insurance, propertyUnitList);
                
                propertyList.add(currentProperty);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PropertyModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return propertyList;
    }

}
