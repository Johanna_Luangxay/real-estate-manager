package com.johanna.realestatemanager.models;

/**
 * Class object of unit of type plexe
 * @author Johanna
 */
public class PlexUnit extends Unit{

    /**
     * Constructor
     * @param unitId
     * @param tenantName
     * @param gasFees
     * @param electricityFees
     */
    public PlexUnit(String unitId, String tenantName, double gasFees, double electricityFees){
        super(unitId, tenantName, gasFees, electricityFees);       
    }
}
