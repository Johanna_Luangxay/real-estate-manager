/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.johanna.realestatemanager.models;

/**
 * Object class that represent a unit of condo type
 * @author Johanna
 */

public class CondoUnit extends Unit {

    private double condoFees;
    
    /**
     * Constructor
     * @param tenantName String name of the tenant
     * @param gasFees double fees for gas
     * @param electricityFees double fees for electricity
     * @param condoFees double fees for condo
     */
    public CondoUnit(String tenantName, double gasFees, double electricityFees, double condoFees){
        //Id is 1 because only 1 unit in condo property
    	super("1",tenantName, gasFees, electricityFees);
        this.condoFees = condoFees;
    }

    /**
     * @return the condoFees
     */
    public double getCondoFees() {
        return condoFees;
    }

    /**
     * @param condoFees the condoFees to set
     */
    public void setCondoFees(double condoFees) {
        this.condoFees = condoFees;
    }
}
