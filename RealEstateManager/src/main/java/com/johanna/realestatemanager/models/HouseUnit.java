package com.johanna.realestatemanager.models;

/**
 * Class object of a unit of type house
 * @author Johanna
 */
public class HouseUnit extends Unit{
    
	/**
	 * Constractor
	 * @param tenantName
	 * @param gasFees
	 * @param electricityFees
	 */
    public HouseUnit(String tenantName, double gasFees, double electricityFees){
    	//Gets id of 1 because only one unit in a house
    	super("1",tenantName, gasFees, electricityFees);
    
    }
}
