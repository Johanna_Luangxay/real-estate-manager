package com.johanna.realestatemanager.models;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utlity class for all database action related and common to most
 * @author Johanna Luangxay
 */
public class DBManager {
    
    public Connection conn = getConnection();
    
    public DBManager() throws SQLException{
    }
    
    /**
     * It gets the connection from the database (my personal local database)
     * @return Connection
     * @throws SQLException
     */
    public Connection getConnection() throws SQLException {
	String user = "root";
	String password = "Joe792002";
        String url = "jdbc:mysql://localhost:3306/real_estate_manager";
	conn = DriverManager.getConnection(url, user, password);
	return conn;
    }
    
    /**
     * Creates all the table need for the application
     */
    public void createAllTables() {
        
        try {
            ArrayList<String> tableNameList = new ArrayList<String>();
            ArrayList<String> valueList = new ArrayList<String>();
            
            tableNameList.add("properties");
            valueList.add("(property_id varchar(10), address varchar(50) ,type varchar(20), "
                    + "unit int, restrictions varchar(500), date_ownership date, "
                    + "ispaid boolean, school_taxe double, property_taxe double, insurance double, "
                    + "PRIMARY KEY(property_id))");
            
            tableNameList.add("tenants");
            valueList.add("(tenant_id varchar(10), first_name varchar(50), last_name varchar(50), "
                    + "property_id varchar(10) NULL, unit_id varchar(10) NULL, email varchar(100), phone varchar(10), "
                    + "monthly_rent double, rent_rate double, "
                    + "PRIMARY KEY(tenant_id), "
                    + "FOREIGN KEY(property_id) REFERENCES properties(property_id) ON DELETE SET NULL)");
            
            tableNameList.add("units");
            valueList.add("(unit_id varchar(10), property_id varchar(10), tenant_id varchar(10) NULL, gas double, "
                    + "electricity double, condoFees double, PRIMARY KEY(unit_id,property_id), "
                    + "FOREIGN KEY(property_id) REFERENCES properties(property_id) ON DELETE CASCADE,"
                    + "FOREIGN KEY(tenant_id) REFERENCES tenants(tenant_id))");
            
            tableNameList.add("payment_history");
            valueList.add("(payment_id varchar(10), tenant_id varchar(10), payDate date, "
                    + "method varchar(20), amount double, PRIMARY KEY(payment_id), FOREIGN KEY(tenant_id) "
                    + "REFERENCES tenants(tenant_id))");
            
            tableNameList.add("financial_institutions");
            valueList.add("(fi_id varchar(10), name varchar(50), interest_rate double, PRIMARY KEY(fi_id))");        
            
            tableNameList.add("mortgages");
            valueList.add("(mortgage_id varchar(10), property_id varchar(10), down_payment double, "
                    + /*"amount_payed double,*/ "property_value double, amount_left double, fi_id varchar(10), interest_rate double, "
                    + "start_mortgage date, end_mortgage date, monthly_payment double, "
                    + "PRIMARY KEY(mortgage_id), FOREIGN KEY(property_id) REFERENCES properties(property_id),"
                    + "FOREIGN KEY(fi_id) REFERENCES financial_institutions(fi_id))");
            
            tableNameList.add("contractors");
            valueList.add("(contractor_id varchar(10), first_name varchar(50), last_name varchar(50), "
                    + "phone varchar(10), email varchar(100), type varchar(20), PRIMARY KEY (contractor_id))");
            
            tableNameList.add("maintenances");
            valueList.add("(maintenance_id varchar(10),contractor_id varchar(10), property_id varchar(10), "
                    + "cost double, appointment_date date, appointment_time time, reason varchar(500), "
                    + "PRIMARY KEY (maintenance_id), FOREIGN KEY(contractor_id) REFERENCES contractors(contractor_id),"
                    + "FOREIGN KEY(property_id) REFERENCES properties(property_id))");
            
            //Create the tables
            Iterator<String> tableName = tableNameList.iterator();
            Iterator<String> value = valueList.iterator();
            while(tableName.hasNext() && value.hasNext()){            
                createTable(tableName.next(), value.next());
            }
            
            //Add foreign key
            String statement = "ALTER TABLE tenants ADD FOREIGN KEY(unit_id) REFERENCES units(unit_id)";
            PreparedStatement ps = conn.prepareStatement(statement);
            ps.executeUpdate();
            
            //Add Data for financial institutions
            populateFi();    
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    /**
     * Create a table based with the table name and the value of the table
     * @param table_name
     * @param value
     * @throws SQLException
     */
    private void createTable(String table_name, String value) throws SQLException{
        String tableStatement = "CREATE TABLE IF NOT EXISTS " + table_name + value; 
        PreparedStatement ps = conn.prepareStatement(tableStatement);
        ps.executeUpdate();
    }
    
    /**
     * Put default data of the financial institutions table
     */
    private void populateFi(){
        try {
            Statement statement = conn.createStatement();
            String insertStat = "INSERT IGNORE INTO financial_institutions VALUES('1','BMO',0.0)";
            statement.executeUpdate(insertStat);
            
            insertStat = "INSERT IGNORE INTO financial_institutions VALUES('2','CIBC',0.0)";
            statement.executeUpdate(insertStat);
            
            insertStat = "INSERT IGNORE INTO financial_institutions VALUES('3','Desjardins',0.0)";
            statement.executeUpdate(insertStat);
            
            insertStat = "INSERT IGNORE INTO financial_institutions VALUES('4','RBC',0.0)";
            statement.executeUpdate(insertStat);
            
            insertStat = "INSERT IGNORE INTO financial_institutions VALUES('5','TD',0.0)";
            statement.executeUpdate(insertStat);
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    /**
     * General method to get lastest id from a table
     * @param field String column of the id
     * @param table String table name
     * @return
     */
    public static int getDBLatestId(String field,String table) {
        int id = 0;
        try {
            ArrayList<Integer> arr = new ArrayList();
            DBManager db = new DBManager();
            String query = "SELECT "+field+" FROM "+table;
            PreparedStatement ps = db.conn.prepareStatement(query);
            ResultSet rps = ps.executeQuery();
            //ArrayList max function get the highest id
            while(rps.next()){
                arr.add(Integer.parseInt(rps.getString(1)));
            }
            if(!arr.isEmpty()){
                id = Collections.max(arr);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }
    
    /**
     * Return String a select statement with one return field
     * @param query
     * @param value
     * @return
     */
    public static String getString(String query, String value) {
        String s = "";
        if(value != null){
            DBManager db;
            try {
                db = new DBManager();
                PreparedStatement ps = db.conn.prepareStatement(query);
                ps.setString(1,value);
                ResultSet rps = ps.executeQuery(); 
                if(rps.next()){
                   s = rps.getString(1); 
                }
            } catch (SQLException ex) {
                Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
            } 

        }
        return s;        
    }
    
    /**
     * Remove an entry in a table 
     * @param id
     * @param field
     * @param table
     */
    public static void removeEntry(String id, String field, String table){
        try {
            DBManager db = new DBManager();
            
            Statement s =  db.conn.createStatement();
            s.executeUpdate("SET FOREIGN_KEY_CHECKS = 0");
            
            String deleteStatement = "DELETE FROM "+table+" WHERE "+field+" LIKE ?";
            PreparedStatement ps =  db.conn.prepareStatement(deleteStatement);
            ps.setString(1,id);
            
            ps.executeUpdate();
            
            s.executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
        } 
        catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Delete all row of a given table
     * @param table
     */
    public static void deleteAll(String table){
        try {
            DBManager db = new DBManager();
            Statement s =  db.conn.createStatement();
            s.executeUpdate("SET FOREIGN_KEY_CHECKS = 0");
            s.executeUpdate("TRUNCATE "+table);
            s.executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
        } catch (SQLException ex) {
            Logger.getLogger(PaymentHistoryModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
