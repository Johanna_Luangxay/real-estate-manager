package com.johanna.realestatemanager.models;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Class of object for maintenance
 * @author Johanna
 */
public class Maintenance {
    private String maintenanceId;
    private String contractorId;
    private String propertyId;
    private double cost;
    private LocalDate date;
    private LocalTime time;
    private String reason;
    
    /**
     * Constructor
     * @param maintenanceId
     * @param contractorId
     * @param propertyId
     * @param cost
     * @param date
     * @param time
     * @param reason
     */
    public Maintenance(String maintenanceId, String contractorId, String propertyId,
            double cost, LocalDate date, LocalTime time, String reason){
        
        this.maintenanceId = maintenanceId;
        this.contractorId = contractorId;
        this.propertyId = propertyId;
        this.cost = cost;
        this.date = date;
        this.time = time;
        this.reason = reason;
        
    }

    /**
     * @return the maintenanceId
     */
    public String getMaintenanceId() {
        return maintenanceId;
    }

    /**
     * @return the contractorId
     */
    public String getContractorId() {
        return contractorId;
    }

    /**
     * @param contractorId the contractorId to set
     */
    public void setContractorId(String contractorId) {
        this.contractorId = contractorId;
    }

    /**
     * @return the propertyId
     */
    public String getPropertyId() {
        return propertyId;
    }

    /**
     * @param propertyId the propertyId to set
     */
    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    /**
     * @return the price
     */
    public double getCost() {
        return cost;
    }

    /**
     * @param price the price to set
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return the time
     */
    public LocalTime getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(LocalTime time) {
        this.time = time;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }
    
}
