package com.johanna.realestatemanager.models;


/**
 * Class of a unit object
 * @author Johanna
 */
public class Unit {
    private String unitId;
    private String tenantName;
    private double gasFees;
    private double electricityFees;
    
    /**
     * Constructor
     * @param unitId
     * @param tenantName
     * @param gasFees
     * @param electricityFees
     */
    public Unit(String unitId, String tenantName, double gasFees, double electricityFees){
        this.unitId = unitId;
        this.tenantName = tenantName;
        this.gasFees = gasFees;
        this.electricityFees = electricityFees;
    }
    
    public String getUnitId(){
        return unitId;
    }
    
    public String getTenantName(){
        return tenantName;
    }
    
    public double getGasFees(){
        return gasFees;
    }
    
    public double getElectricityFees(){
        return electricityFees;
    }
    
    public void setUnitId(String unitId){
        this.unitId = unitId;
    }
    
    public void setTenantName(String tenantName){
        this.tenantName = tenantName;
    }
    
    public void setGasFees(double gasFees){
        this.gasFees = gasFees;
    }
    
    public void setElectricityFees(double electricityFees){
        this.electricityFees = electricityFees;
    }
}
