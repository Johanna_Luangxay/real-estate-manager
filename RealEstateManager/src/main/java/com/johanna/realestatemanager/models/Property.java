package com.johanna.realestatemanager.models;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Class  object for property
 * @author Johanna
 */
public class Property {
    
    private final String id;
    private String address;
    private String type;
    private int unit;
    private String restrictions;
    private LocalDate dateOfOwnership;
    private boolean isPaid;
    private double schoolTaxe;
    private double propertyTaxe;
    private double insurance;
    private ArrayList<Unit> propertyUnit;
    
    /**
     * Constructor
     * @param id String
     * @param address String
     * @param type String
     * @param unit int
     * @param restrictions String
     * @param dateOfOwnership LocalDate
     * @param isPaid boolean
     * @param schoolTaxe double
     * @param propertyTaxe double
     * @param insurance double
     * @param propertyUnit ArrayList
     */
    public Property(String id, String address, String type, int unit, String restrictions, 
            LocalDate dateOfOwnership, boolean isPaid, double schoolTaxe, double propertyTaxe, 
            double insurance, ArrayList<Unit> propertyUnit){
        this.id = id;
        this.address = address;
        this.type = type;
        this.unit = unit;
        this.restrictions = restrictions;
        this.dateOfOwnership = dateOfOwnership;
        this.isPaid = isPaid;
        this.schoolTaxe = schoolTaxe;
        this.propertyTaxe = propertyTaxe;
        this.insurance = insurance;
        this.propertyUnit = propertyUnit;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    
    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the unit
     */
    public int getUnit() {
        return unit;
    }

    /**
     * @param unit the unit to set
     */
    public void setUnit(int unit) {
        this.unit = unit;
    }

    /**
     * @return the restrictions
     */
    public String getRestrictions() {
        return restrictions;
    }

    /**
     * @param restrictions the restrictions to set
     */
    public void setRestrictions(String restrictions) {
        this.restrictions = restrictions;
    }

    /**
     * @return the dateOfOwnership
     */
    public LocalDate getDateOfOwnership() {
        return dateOfOwnership;
    }

    /**
     * @param dateOfOwnership the dateOfOwnership to set
     */
    public void setDateOfOwnership(LocalDate dateOfOwnership) {
        this.dateOfOwnership = dateOfOwnership;
    }

    /**
     * @return the isPaid
     */
    public boolean isIsPaid() {
        return isPaid;
    }

    /**
     * @param isPaid the isPaid to set
     */
    public void setIsPaid(boolean isPaid) {
        this.isPaid = isPaid;
    }

    /**
     * @return the schoolTaxe
     */
    public double getSchoolTaxe() {
        return schoolTaxe;
    }

    /**
     * @param schoolTaxe the schoolTaxe to set
     */
    public void setSchoolTaxe(double schoolTaxe) {
        this.schoolTaxe = schoolTaxe;
    }

    /**
     * @return the propertyTaxe
     */
    public double getPropertyTaxe() {
        return propertyTaxe;
    }

    /**
     * @param propertyTaxe the propertyTaxe to set
     */
    public void setPropertyTaxe(double propertyTaxe) {
        this.propertyTaxe = propertyTaxe;
    }

    /**
     * @return the insurance
     */
    public double getInsurance() {
        return insurance;
    }

    /**
     * @param insurance the insurance to set
     */
    public void setInsurance(double insurance) {
        this.insurance = insurance;
    }
    
    /**
     * Convert the payment status into word
     * @param isPaid
     * @return
     */
    public static String convertStatusToWord(Boolean isPaid){
        if (isPaid){
            return "Paid";
        } else {
            return "Unpaid";
        }
    }

    /**
     * @return the propertyUnit
     */
    public ArrayList<Unit> getPropertyUnit() {
        return propertyUnit;
    }

    /**
     * @param propertyUnit the propertyUnit to set
     */
    public void setPropertyUnit(ArrayList<Unit> propertyUnit) {
        this.propertyUnit = propertyUnit;
    }  
    
}
