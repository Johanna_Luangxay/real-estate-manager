/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.johanna.realestatemanager.models;

/**
 *
 * @author Johanna
 */
public class Contractor {
    private String contractorId;
    private String fname;
    private String lname;
    private String phone;
    private String email;
    private String type;
    
    /**
     * Constractor
     * @param contractorId
     * @param fname
     * @param lname
     * @param phone
     * @param email
     * @param type
     */
    public Contractor(String contractorId, String fname, String lname, String phone, String email, String type){
        this.contractorId = contractorId;
        this.fname = fname;
        this.lname = lname;
        this.phone = phone;
        this.email = email;
        this.type = type;
    }

    /**
     * Gets the fullname of the contractor
     * @return String fullname
     */
    public String getFullName(){
        return fname+" "+lname;
    }
    
    /**
     * @return the contractorId
     */
    public String getContractorId() {
        return contractorId;
    }

    /**
     * @return the fname
     */
    public String getFname() {
        return fname;
    }

    /**
     * @param fname the fname to set
     */
    public void setFname(String fname) {
        this.fname = fname;
    }

    /**
     * @return the lname
     */
    public String getLname() {
        return lname;
    }

    /**
     * @param lname the lname to set
     */
    public void setLname(String lname) {
        this.lname = lname;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    
    
}
