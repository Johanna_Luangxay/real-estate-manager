/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.johanna.realestatemanager.models;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Johanna
 */
public class CondoUnitTest {
    
    public CondoUnitTest() {
    }

    /**
     * Test of getCondoFees method, of class CondoUnit.
     */
    @Test
    public void testGetCondoFees() {
        CondoUnit instance = new CondoUnit("William Smith", 15.65,35.75,95.45);
        double expResult = 95.45;
        double result = instance.getCondoFees();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCondoFees method, of class CondoUnit.
     */
    @Test
    public void testSetCondoFees() {
        double condoFees = 35.15;
        CondoUnit instance = new CondoUnit("William Smith", 15.65,35.75,95.45);
        instance.setCondoFees(condoFees);
        
        double expResult = 35.15;
        double result = instance.getCondoFees();
        assertEquals(expResult, result);
    }
    
}
