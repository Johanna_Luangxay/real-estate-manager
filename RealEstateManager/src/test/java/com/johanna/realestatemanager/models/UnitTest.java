/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.johanna.realestatemanager.models;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Johanna
 */
public class UnitTest {
    
    
    public UnitTest() {
    }

    /**
     * Test of getUnitId method, of class Unit.
     */
    @org.junit.jupiter.api.Test
    public void testGetUnitId() {
        Unit unit1 = new Unit("1","Liam Noah",0,0);
        Unit unit2 = new Unit("2",null,300,100);
        
        String expResult = "1";
        String result = unit1.getUnitId();
        assertEquals(expResult, result);   
        
        expResult = "2";
        result = unit2.getUnitId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTenantName method, of class Unit.
     */
    @Test
    public void testGetTenantName() {
        Unit unit1 = new Unit("1","Liam Noah",0,0);
        Unit unit2 = new Unit("2",null,300,100);
        
        String expResult = "Liam Noah";
        String result = unit1.getTenantName();
        assertEquals(expResult, result);
        
        expResult = null;
        result = unit2.getTenantName();
        assertEquals(expResult, result);        
    }

    /**
     * Test of getGasFees method, of class Unit.
     */
    @Test
    public void testGetGasFees() {
        Unit unit1 = new Unit("1","Liam Noah",0,0);
        Unit unit2 = new Unit("2",null,300,100);
        
        double expResult = 0;
        double result = unit1.getGasFees();
        assertEquals(expResult, result);
        
        expResult = 300;
        result = unit2.getGasFees();
        assertEquals(expResult, result);        
    }

    /**
     * Test of getElectricityFees method, of class Unit.
     */
    @Test
    public void testGetElectricityFees() {
        Unit unit1 = new Unit("1","Liam Noah",0,0);
        Unit unit2 = new Unit("2",null,300,100);
        
        double expResult = 0;
        double result = unit1.getElectricityFees();
        assertEquals(expResult, result);
        
        expResult = 100;
        result = unit2.getElectricityFees();
        assertEquals(expResult, result);
    }

    /**
     * Test of setUnitId method, of class Unit.
     */
    @Test
    public void testSetUnitId() {
        Unit unit = new Unit(null,null,0,0);
        
        String unitId = "3";
        unit.setUnitId(unitId);
        
        String expResult = "3";
        String result = unit.getUnitId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTenantName method, of class Unit.
     */
    @Test
    public void testSetTenantName() {
        Unit unit = new Unit(null,null,0,0);
        
        String tenantName = "Erza Scarlett";
        unit.setTenantName(tenantName);
        
        String expResult = "Erza Scarlett";
        String result = unit.getTenantName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setGasFees method, of class Unit.
     */
    @Test
    public void testSetGasFees() {
        Unit unit = new Unit(null,null,0,0);
        
        double gasFees = 200;
        unit.setGasFees(gasFees);
        
        double expResult = 200;
        double result = unit.getGasFees();
        assertEquals(expResult, result);
    }

    /**
     * Test of setElectricityFees method, of class Unit.
     */
    @Test
    public void testSetElectricityFees() {
        Unit unit = new Unit(null,null,0,0);
        
        double electricityFees = 800;
        unit.setElectricityFees(electricityFees);
        
        double expResult = 800;
        double result = unit.getElectricityFees();
        assertEquals(expResult, result);
    }    
}
